// Get index of latest view to configure
v0 = PostProcessing.NbViews-1;

View[v0].Axes = 5;
View[v0].Color.Axes = Black;
//2=continuous
View[v0].IntervalsType = 2 ;
//4=2D plot
View[v0].Type = 4;
View[v0].IntervalsType = 2 ;
View[v0].Visible = 1;

View[v0].AutoPosition = 0;
View[v0].PositionX = 85;
View[v0].PositionY = 50;
View[v0].Width = 400;
View[v0].Height = 260;
