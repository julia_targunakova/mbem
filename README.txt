### 1. Brief description
    MBEM is a software package for solving magnetic problems of the thin-walled
    structures in a three-dimensional space.

    MBEM stands for Modified Boundary Elements Method. Unlike the classical BEM
    which required to have Green's function (thus limiting geometry to simple
    cases whenever Green's function is known or can be found) MBEM allows to
    solve magnetic problems for objects with arbitrary geometry. To achieve this
    the universal coefficients of influence were developed.

    Comparing to Finite Elements Method (FEM) and Finite Difference Method (FDM)
    MBEM doesn't require to introduce synthetic boundaries allowing to
    calculate field values at any point of area. Also, MBEM requires to make
    sampling only of the object surface instead of the whole area thus
    significantly reducing complexity of equations to be solved.

    MBEM consists of the following components:

        1. mbem.exe - coordinates the work between all the programs that are
           included in the software package and connects MBEM with gmsh (.exe
           extension is added for better integration with gmsh).

        2. mbem_potential - calculates the potential of a simple layer of a
           thin-walled object and saves the result to a result.txt file.

        3. mbem_field - generates files with calculated values of the tension
           vector, magnetic potential and magnetic induction using result.txt.

### 2. Coding style
    MBEM project follows QEMU coding style.
    https://gitlab.com/storedmirrors/qemu/blob/master/CODING_STYLE

### 3. Build and test

# To build type
$ make
or
$ make all

# To build with debug options type
$ make DEBUG=1

# To build test meshes type
$ make tests

# To run field calculation for each test mesh type
$ make check

# To see results of each test mesh calculation use NORUN option.
# Type and press RUN manually
$ make check NORUN=1

### 4. System requirements
    * Linux-based operating system:
        * Ubuntu 16.04
        * Ubuntu 18.04
        * Debian 8 (Jessie)
        * Other distributions may be fine, but not tested
    * Additional packages:
        * gmsh
        * libgsl
        * libblas

