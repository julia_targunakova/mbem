#
#    Copyright (C) (2018) Julia Targunakova <julia.targunakova@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

CC=gcc
CPP=g++
CFLAGS=-Wall
LDFLAGS=

DEBUG ?= 0
NORUN ?= 0

ifeq ($(DEBUG),1)
  $(info Will compile with debug options)
  CFLAGS += -g -O0
else
  CFLAGS += -O2
endif

ifeq ($(V),1)
  Q:=
  GMSH_V:=4
else
  Q:=@
  GMSH_V:=0
endif

GMSH=gmsh

DIST_DIR=dist
TESTS_DIR=$(DIST_DIR)/tests

MBEM_CONNECTOR=mbem.exe
MBEM_POTENTIAL=mbem_potential
MBEM_FIELD=mbem_field

BINS = $(DIST_DIR)/$(MBEM_CONNECTOR)
BINS += $(DIST_DIR)/$(MBEM_POTENTIAL)
BINS += $(DIST_DIR)/$(MBEM_FIELD)

all: $(BINS) extra

phony += all

$(DIST_DIR):
	$(Q)mkdir $(DIST_DIR)

$(TESTS_DIR): | $(DIST_DIR)
	$(Q)mkdir $(TESTS_DIR)


mbem_connector.o: mbem_connector.cpp onelab.h GmshSocket.h
	$(Q)echo "  $@"
	$(Q)$(CPP) $(CFLAGS) -c $< -o $@

$(DIST_DIR)/$(MBEM_CONNECTOR): mbem_connector.o | $(DIST_DIR)
	$(Q)echo "  $@"
	$(Q)$(CPP) $(LDFLAGS) $^ -o $@


mbem_potential.o: mbem_potential.c
	$(Q)echo "  $@"
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

$(DIST_DIR)/$(MBEM_POTENTIAL): mbem_potential.o | $(DIST_DIR)
	$(Q)echo "  $@"
	$(Q)$(CC) $(LDFLAGS) $^ -o $@ -lgsl -lblas -lm -lpthread


mbem_field.o: mbem_field.c
	$(Q)echo "  $@"
	$(Q)$(CC) $(CFLAGS) -c $< -o $@

$(DIST_DIR)/$(MBEM_FIELD): mbem_field.o | $(DIST_DIR)
	$(Q)echo "  $@"
	$(Q)$(CC) $(LDFLAGS) $^ -o $@ -lm

clean:
	$(Q)rm -f *.o
	$(Q)rm -rf $(DIST_DIR)
	
phony += clean


EXTRA += $(DIST_DIR)/plot2D.geo
EXTRA += $(DIST_DIR)/plot3D.geo

# extra target is to dist additional files
extra: $(EXTRA)
phony += extra

$(DIST_DIR)/plot%.geo: plot%.geo | $(DIST_DIR)
	$(Q)echo "  $@"
	$(Q)cp $< $@


# Test geometries list
TESTS += $(TESTS_DIR)/sphere.msh
TESTS += $(TESTS_DIR)/yacht_42.msh

tests: $(TESTS)

# Rule to make meshes using gmsh
$(TESTS_DIR)/%.msh: tests/gmsh_geometries/%.geo | $(DIST_DIR) $(TESTS_DIR)
	$(Q)echo "  $@"
	$(Q)$(GMSH) -2 -v $(GMSH_V) $< -o $@

$(TESTS_DIR)/%.msh: tests/gmsh_geometries/%.brep | $(DIST_DIR) $(TESTS_DIR)
	$(Q)echo "  $@"
	$(Q)$(GMSH) -2 -v $(GMSH_V) $< -o $@

phony += tests

# run field calculcation for each test mesh
check:
	$(Q)./check.sh $(GMSH) $(DIST_DIR)/$(MBEM_CONNECTOR) $(NORUN) $(TESTS_DIR) "$(TESTS)"

phony += check

.PHONY: $(phony)
