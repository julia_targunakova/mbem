/*
 * Copyright (C) (2018) Julia Targunakova <julia.targunakova@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <libgen.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "onelab.h"

#define LOG(fmt, ...) do {                          \
    if (log_all)                                    \
        fprintf(log_all, fmt, ## __VA_ARGS__);      \
    if (log_last)                                   \
        fprintf(log_last, fmt, ## __VA_ARGS__);     \
    printf(fmt, ## __VA_ARGS__);                    \
} while (0)

static FILE *log_all;
static FILE *log_last;

static int log_init(void)
{
    log_all = fopen("log_all", "a");
    if (!log_all) {
        fprintf(stderr, "Can't open log_all - %d\n", errno);
        return -1;
    }

    log_last = fopen("log_last", "w");
    if (!log_last) {
        fprintf(stderr, "Can't open log_last - %d\n", errno);
        fclose(log_all);
        log_all = NULL;
        return -1;
    }

    return 0;
}

static char *get_app_path(char *path_to_app)
{
    char *path;
    path = strdup(path_to_app);
    if (!path) {
        int saved_errno = errno;
        LOG("Error: strdup() failed - %d\n", saved_errno);
    }

    path = dirname(path);

    return path;
}

int main(int argc, char **argv)
{
    int ret = 0;
    char str[256];
    char *path;
    std::string name, address;
    unsigned int flag_txt = 0;
    unsigned int flag_pos = 0;
    unsigned int ambient_field = 0;
    unsigned int graph_result_type = 0;
    unsigned int flag_RUN_mode_calc = 0;
    unsigned int flag_RUN_mode_gener = 0;
    /* In values */
    double Hx, Hy, Hz;
    double permeability;
    double thickness;
    double position_x;
    double position_y;
    double position_z;
    double size_length;
    double size_width;
    int nPoint_l;
    int nPoint_w;
    int nPoint_graph;
    double position_line_width;

    if (log_init()) {
        return 1;
    }

    LOG("MBEM Connector started\n");

    path = get_app_path(argv[0]);
    if (!path) {
        return 1;
    }

    for (int i = 0; i < argc; i++) {
        LOG("arg[%d] = %s\n", i, argv[i]);
        if (std::string(argv[i]) == "-onelab" && i + 2 < argc) {
            name = std::string(argv[i + 1]);
            address = std::string(argv[i + 2]);
        }
    }

    if (name.empty() || address.empty()) {
        LOG("Run incorrectly\n");
        return 1;
    }

    onelab::remoteNetworkClient *c = new onelab::remoteNetworkClient(name,
            address);

    std::vector<onelab::number> ns_thickness;
    c->get(ns_thickness, "MBEM/IN/Thickness");
    if (ns_thickness.empty()) {
        onelab::number n("MBEM/IN/Thickness", 0.1);
        thickness = 0.1; /* Default value */
        c->set(n);
    } else {
        thickness = ns_thickness[0].getValue();
    }

    std::vector<onelab::number> ns_number;
    c->get(ns_number, "MBEM/IN/Hx");
    if (ns_number.empty()) {
        onelab::number n("MBEM/IN/Hx", 0.0);
        Hx = 0.0; /* Default value */
        c->set(n);
    } else {
        Hx = ns_number[0].getValue();
    }
    LOG("MBEM/IN/Hx = %f\n", Hx);

    c->get(ns_number, "MBEM/IN/Hy");
    if (ns_number.empty()) {
        onelab::number n("MBEM/IN/Hy", 0.0);
        Hy = 0.0; /* Default value */
        c->set(n);
    } else {
        Hy = ns_number[0].getValue();
    }
    LOG("MBEM/IN/Hy = %f\n", Hy);

    c->get(ns_number, "MBEM/IN/Hz");
    if (ns_number.empty()) {
        onelab::number n("MBEM/IN/Hz", -1.0);
        Hz = -1.0; /* Default value */
        c->set(n);
    } else {
        Hz = ns_number[0].getValue();
    }
    LOG("MBEM/IN/Hz = %f\n", Hz);

    c->get(ns_number, "MBEM/IN/Permeability");
    if (ns_number.empty()) {
        onelab::number n("MBEM/IN/Permeability", 150.0);
        permeability = 150.0; /* Default value */
        c->set(n);
    } else {
        permeability = ns_number[0].getValue();
    }
    LOG("MBEM/IN/Permeability = %f\n", permeability);

    std::vector<onelab::string> ns;
    std::string out_ambient_field;
    c->get(ns, "MBEM/OUT/Ambient field");
    if (ns.empty()) {
        onelab::string n("MBEM/OUT/Ambient field", "yes");
        out_ambient_field = "yes"; /* Default value */
        std::vector<std::string> labels;
        labels.push_back("yes");
        labels.push_back("no");
        n.setChoices(labels);
        c->set(n);
    } else {
        out_ambient_field = ns[0].getValue();
    }
    LOG("MBEM/OUT/Ambient field: %s\n", out_ambient_field.c_str());
    if (out_ambient_field == "yes") {
        ambient_field = 1;
    } else if (out_ambient_field == "no") {
        ambient_field = 0;
    } else {
        LOG("ERROR: MBEM/OUT/Ambient field has invalid value\n");
        delete c;
        return 1;
    }
    LOG("  ambient_field = %u\n", ambient_field);

    std::string out_file_format;
    c->get(ns, "MBEM/OUT/Output file format");
    if (ns.empty()) {
        onelab::string n("MBEM/OUT/Output file format", ".pos");
        out_file_format = ".pos"; /* Default value */
        std::vector<std::string> labels;
        labels.push_back(".txt");
        labels.push_back(".pos");
        labels.push_back("ALL");
        n.setChoices(labels);
        c->set(n);
    } else {
        out_file_format = ns[0].getValue();
        printf("read existing out file format successful\n");
    }
    LOG("Output file format: %s\n", out_file_format.c_str());
    if (out_file_format == ".txt") {
        flag_txt = 1;
    } else if (out_file_format == ".pos") {
        flag_pos = 1;
    } else if (out_file_format == "ALL") {
        flag_txt = 1;
        flag_pos = 1;
    } else {
        printf("Error: invalid out_file_format = %s\n", out_file_format.c_str());
        delete c;
        return 1;
    }
    LOG("flag_txt = %d, flag_pos = %d\n", flag_txt, flag_pos);

    std::vector<onelab::number> ns_position_plane;
    c->get(ns_position_plane, "MBEM/OUT/PLANE/start position/x");
    if (ns_number.empty()) {
        onelab::number n("MBEM/OUT/PLANE/start position/x", -5.0);
        position_x = -5; /* Default value */
        c->set(n);
    } else {
        position_x = ns_position_plane[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/start position/x = %f\n", position_x);

    c->get(ns_position_plane, "MBEM/OUT/PLANE/start position/y");
    if (ns_number.empty()) {
        onelab::number n("MBEM/OUT/PLANE/start position/y", -5.0);
        position_y = -5; /* Default value */
        c->set(n);
    } else {
        position_y = ns_position_plane[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/start position/y = %f\n", position_y);

    c->get(ns_position_plane, "MBEM/OUT/PLANE/start position/z");
    if (ns_number.empty()) {
        onelab::number n("MBEM/OUT/PLANE/start position/z", -2);
        position_z = -2; /* Default value */
        c->set(n);
    } else {
        position_z = ns_position_plane[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/start position/z = %f\n", position_z);

    std::vector<onelab::number> ns_size_length;
    c->get(ns_size_length, "MBEM/OUT/PLANE/Size length");
    if (ns_size_length.empty()) {
        onelab::number n("MBEM/OUT/PLANE/Size length", 10);
        size_length = 10; /* Default value */
        c->set(n);
    } else {
        size_length = ns_size_length[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/Size length = %f\n", size_length);

    std::vector<onelab::number> ns_size_width;
    c->get(ns_size_width, "MBEM/OUT/PLANE/Size width");
    if (ns_size_width.empty()) {
        onelab::number n("MBEM/OUT/PLANE/Size width", 10);
        size_width = 10; /* Default value */
        c->set(n);
    } else {
        size_width = ns_size_width[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/Size width = %f\n", size_width);

    std::vector<onelab::number> ns_nPoint_length;
    c->get(ns_nPoint_length, "MBEM/OUT/PLANE/nPoint_length");
    if (ns_nPoint_length.empty()) {
        onelab::number n("MBEM/OUT/PLANE/nPoint_length", 81);
        nPoint_l = 81; /* Default value */
        c->set(n);
    } else {
        nPoint_l = ns_nPoint_length[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/nPoint_length = %u\n", nPoint_l);

    std::vector<onelab::number> ns_nPoint_width;
    c->get(ns_nPoint_width, "MBEM/OUT/PLANE/nPoint_width");
    if (ns_nPoint_width.empty()) {
        onelab::number n("MBEM/OUT/PLANE/nPoint_width", 41);
        nPoint_w = 41; /* Default value */
        c->set(n);
    } else {
        nPoint_w = ns_nPoint_width[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/nPoint_width = %u\n", nPoint_w);

    std::vector<onelab::number> ns_nPoint_graph;
    c->get(ns_nPoint_graph, "MBEM/OUT/PLANE/GRAPH nPoint");
    if (ns_nPoint_graph.empty()) {
        onelab::number n("MBEM/OUT/PLANE/GRAPH nPoint", 161);
        nPoint_graph = 161; /* Default value */
        c->set(n);
    } else {
        nPoint_graph = ns_nPoint_graph[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/GRAPH nPoint = %u\n", nPoint_graph);

    std::string Axis_plane;
    c->get(ns, "MBEM/OUT/PLANE/Axis");
    if (ns.empty()) {
        onelab::string n("MBEM/OUT/PLANE/Axis", "Z");
        Axis_plane = "Z"; /* Default value */
        std::vector<std::string> labels;
        labels.push_back("Z");
        labels.push_back("X");
        labels.push_back("Y");
        n.setChoices(labels);
        c->set(n);
    } else {
        Axis_plane = ns[0].getValue();
    }
    LOG("Axis_plane: %s\n", Axis_plane.c_str());

    std::vector<onelab::number> ns_position_line_width_plane;
    c->get(ns_position_line_width_plane, "MBEM/OUT/PLANE/GRAPH position width");
    if (ns_position_line_width_plane.empty()) {
        onelab::number n("MBEM/OUT/PLANE/GRAPH position width", 0);
        position_line_width = 0; /* Default value */
        c->set(n);
    } else {
        position_line_width = ns_position_line_width_plane[0].getValue();
    }
    LOG("MBEM/OUT/PLANE/GRAPH position width = %f\n", position_line_width);

    std::string graph_result;
    c->get(ns, "MBEM/OUT/PLANE/GRAPH result");
    if (ns.empty()) {
        onelab::string n("MBEM/OUT/PLANE/GRAPH result", "intensity");
        graph_result = "intensity"; /* Default value */
        std::vector<std::string> labels;
        labels.push_back("intensity");
        labels.push_back("potential");
        labels.push_back("induction");
        n.setChoices(labels);
        c->set(n);
    } else {
        graph_result = ns[0].getValue();
    }
    LOG("graph_result: %s\t", graph_result.c_str());
    if (graph_result == "intensity") {
        graph_result_type = 0;
    } else if (graph_result == "potential") {
        graph_result_type = 1;
    } else if (graph_result == "induction") {
        graph_result_type = 2;
    } else {
        LOG("Error: invalid graph_result = %s\n", graph_result.c_str());
        delete c;
        return 1;
    }
    LOG("result_graph_type = %u\n", graph_result_type);

    std::string RUN_mode;
    c->get(ns, "MBEM/RUN_mode/RUN_mode");
    if (ns.empty()) {
        onelab::string n("MBEM/RUN_mode/RUN_mode",
                "calculation and generate output"); /* Default value */
        RUN_mode = "generate output";
        std::vector<std::string> labels;
        labels.push_back("calculation");
        labels.push_back("generate output");
        labels.push_back("calculation and generate output");
        n.setChoices(labels);
        c->set(n);
    } else {
        RUN_mode = ns[0].getValue();
    }
    LOG("RUN_mode: %s\t", RUN_mode.c_str());
    if (RUN_mode == "calculation") {
        flag_RUN_mode_calc = 1;
    } else if (RUN_mode == "generate output") {
        flag_RUN_mode_gener = 1;
    } else if (RUN_mode == "calculation and generate output") {
        flag_RUN_mode_calc = 1;
        flag_RUN_mode_gener = 1;
    } else {
        printf("Error: invalid RUN_mode = %s\n", RUN_mode.c_str());
    }
    LOG("flag_RUN_mode_calc = %u,\tflag_RUN_mode_gener = %u\n",
            flag_RUN_mode_calc, flag_RUN_mode_gener);

    std::string action;
    c->get(ns, name + "/Action");
    if (ns.size()) {
        action = ns[0].getValue();
    }
    LOG("action: %s\n", action.c_str());
    if (action != "compute") {
        LOG("action = %s\n", action.c_str());
        LOG("App closing\n");
        delete c;
        return 0;
    }

    std::string mesh_name;
    c->get(ns, "Gmsh/MshFileName");
    // read Gmsh/MshFileName into mesh_name
    if (ns.size()) {
        mesh_name = ns[0].getValue();
        LOG("mesh_name: %s\n", mesh_name.c_str());
    }

    if (flag_RUN_mode_calc == 1) {
        sprintf(str, "%s/mbem_potential -i1 -t%d -p%d -x%f -y%f -z%f -e%f -h%f "
                "-m%d -a%f -b%f -c%f -l%f -w%f -N%d -n%d -A%s -g%f -r%d -u%u %s",
                path, flag_txt, flag_pos, Hx, Hy, Hz, permeability, thickness,
                ambient_field, position_x, position_y, position_z, size_length,
                size_width, nPoint_l, nPoint_w, Axis_plane.c_str(), position_line_width,
                nPoint_graph, graph_result_type, mesh_name.c_str());
    } else if (flag_RUN_mode_gener == 1) {
        char *fresult;
        char *tmp1;
        char *tmp2 = strdup(mesh_name.c_str());
        tmp1 = basename(tmp2);
        unsigned int i = 0;
        while (tmp1[i] != '.') {
            tmp2[i] = tmp1[i];
            i++;
        }
        tmp2[i] = '\0';
        printf("tmp2 = %s\n", tmp2);

        unsigned int len;
        len = strlen(tmp2);
        len += 12;
        fresult = (char *) malloc(len);
        sprintf(fresult, "result_%s.txt", tmp2);

        sprintf(str, "%s/mbem_field -m%u -t%u -p%u -x%f -y%f -z%f -a%f -b%f -c%f -l%f -w%f "
                "-N%d -n%d -A%s -g%g -r%d -u%u %s", path, ambient_field, flag_txt, flag_pos, Hx, Hy, Hz,
                position_x, position_y, position_z, size_length, size_width, nPoint_l, nPoint_w,
                Axis_plane.c_str(), position_line_width, nPoint_graph, graph_result_type, fresult);
    }
    LOG("%s\n", str);

    do {
        ret = system(str);
        printf("mbem_connector: system() ret = %d\n", ret);
        ret = WEXITSTATUS(ret);
        printf("Exit status = %d\n", ret);

        if (WIFSIGNALED(ret)
                && (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT)) {
            break;
        }
    } while (0);

    if (ret == 0) {
        if (flag_RUN_mode_gener == 1) {
            if (flag_pos == 1) {
                if (graph_result_type == 0) {
                    sprintf(str, "intensity_line_overlays_out.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);
                } else if (graph_result_type == 1) {
                    sprintf(str, "potential_line_overlays_out.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);
                } else if (graph_result_type == 2) {
                    sprintf(str, "induction_line_overlays_out.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "induction_graph_x.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "induction_graph_y.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "induction_graph_z.pos");
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);

                    sprintf(str, "%s/plot2D.geo", path);
                    LOG("Merge request file: %s\n", str);
                    c->sendMergeFileRequest(str);
                }

                if (Axis_plane == "Z" || Axis_plane == "z") {
                    sprintf(str, "intensity_field_overlays_x_y.pos");
                }
                if (Axis_plane == "X" || Axis_plane == "x") {
                    sprintf(str, "intensity_field_overlays_z_y.pos");
                }
                if (Axis_plane == "Y" || Axis_plane == "y") {
                    sprintf(str, "intensity_field_overlays_z_x.pos");
                }
                LOG("Merge request file: %s\n", str);
                c->sendMergeFileRequest(str);
                /* Merge view settings for 3D view */
                sprintf(str, "%s/plot3D.geo", path);
                LOG("Merge request file: %s\n", str);
                c->sendMergeFileRequest(str);
            }
        }
    } else {
        printf("mbem_connector: ret = %d\n", ret);
        unlink("intensity_field_overlays_x_y.pos");
        unlink("intensity_field_overlays_z_y.pos");
        unlink("intensity_field_overlays_z_x.pos");
        unlink("file_graph_intensity_pos");
        unlink("file_graph_potential_pos");
        unlink("file_graph_intensity_txt");
        unlink("file_graph_potential_txt");
    }

    LOG("App closing\n\n");

    if (ret) {
        c->sendError("MBEM: Calculation field failed");
    }

    delete c;

    return ret;
}
