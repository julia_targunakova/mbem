#!/bin/bash

#    Copyright (C) (2018) Julia Targunakova <julia.targunakova@gmail.com>
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

PRJ_PATH=$(pwd)

GMSH=$1
MBEM_CONNECTOR=$2
NORUN=$3
TESTS_DIR=$4
TESTS=$5

GMSH_FLAGS="-string Solver.AutoMesh=0;"

if [[ -z $GMSH ]]; then
    echo "GMSH environment is not set"
    exit 1
fi

if [[ -z $GMSH_FLAGS ]]; then
    echo "GMSH_FLAGS environment is not set"
    exit 1
fi

if [[ -z $MBEM_CONNECTOR ]]; then
    echo "MBEM_CONNECTOR environment is not set"
    exit 1
fi

if [[ -z $NORUN ]]; then
    echo "NORUN environment is not set"
    exit 1
fi

pushd $TESTS_DIR || exit 1

RUN_OPTION="-run"
if [[ $NORUN -ne 0 ]]; then
    echo "    ... auto RUN disabled. Press RUN to make calculation and see results"
    RUN_OPTION=""
fi

echo "TESTS=$TESTS"

for i in $TESTS ; do
    MESH_FILE=$(basename $i)
    echo ">>> Opening $MESH_FILE"
    if [[ ! -f $MESH_FILE ]]; then
        echo "Error: File $i not found"
        exit 1
    fi

    WORK_DIR="check_$MESH_FILE"
    [[ ! -d $WORK_DIR ]] && mkdir $WORK_DIR
    
    cp -f $MESH_FILE $WORK_DIR/$MESH_FILE

    pushd $WORK_DIR
    pwd
    $GMSH $GMSH_FLAGS $PRJ_PATH/$MBEM_CONNECTOR $MESH_FILE $RUN_OPTION
    LAST_ERROR=$?
    popd

    if [ $LAST_ERROR -ne 0 ]; then
        echo "Error: GMSH exited with error $LAST_ERROR"
        exit 1
    else
        echo "    ... SUCCEEDED"
    fi
    echo "<<< Done with $MESH_FILE"
done

popd

