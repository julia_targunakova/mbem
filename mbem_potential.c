/*
 * Copyright (C) (2018) Julia Targunakova <julia.targunakova@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <errno.h>
#include <fenv.h>
#include <float.h>
#include <getopt.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <libgen.h>
#include <limits.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_ADJACENT_FACES 100

#define FORMAT_TYPE_GMSH_MSH 1
#define FORMAT_TYPE_ANI3D_OUT 0

long double *A = NULL;
long double *A_valid = NULL;
long double *C = NULL;
long double *C_valid = NULL;

struct vertex_simple {
    long double x;
    long double y;
    long double z;
};

struct vector_simple {
    long double x;
    long double y;
    long double z;
};

struct for_calculate_A {
    unsigned int cnt;
    unsigned int divider;
    unsigned int *face;
    unsigned int nV;
    double *vertex;
    struct adjacent_faces *vertexes_belong_to_faces;
    long double *A;
    long double perm;
};

struct for_calculate_C {
    unsigned int cnt;
    unsigned int divider;
    unsigned int *face;
    unsigned int nV;
    double *vertex;
    struct adjacent_faces *vertexes_belong_to_faces;
    struct vertex_simple *H;
    long double *C;
    long double perm;
};

struct adjacent_faces {
    unsigned int faces[MAX_ADJACENT_FACES];
    unsigned int cnt;
};

static void usage(void)
{
    printf("Usage:\n");
    printf("  ./mbem_potential <options> <mesh_file>\n");
}

double *get_coord_vertexes_from_gmsh_msh(FILE *file, unsigned int *nV_out)
{
    char buf[256];
    unsigned int nV = 0;
    unsigned int i;
    double *vertex;
    double x, y, z;
    int id;

    assert(file);
    assert(nV_out);

    for (i = 0; i < 5; i++) {
        if (!fgets(buf, 256, file)) {
            goto bad_file;
        }
    }
    if (sscanf(buf, "%u", &nV) != 1) {
        goto bad_file;
    }
    printf("%s(): Getting %u vertexes\n", __func__, nV);

    vertex = malloc(nV * sizeof(double) * 3);
    if (!vertex) {
        printf("Error: %s(): failed to allocate memory for vertexes\n",
                __func__);
        return NULL;
    }

    for (i = 0; i < nV; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_malloc;
        }
        if (sscanf(buf, "%d %lf %lf %lf", &id, &x, &y, &z) != 4) {
            goto out_malloc;
        }
        vertex[3 * i + 0] = x;
        vertex[3 * i + 1] = y;
        vertex[3 * i + 2] = z;
    }

    *nV_out = nV;

    return vertex;

out_malloc:
    free(vertex);

bad_file:
    printf("Error: %s(): bad input file format\n", __func__);

    return NULL;
}

double *get_coord_vertexes_from_ani3d_out(FILE *file, unsigned int *nV_out)
{
    char buf[256];
    unsigned int nV = 0;
    unsigned int i;
    double *vertex;
    double x, y, z;

    assert(file);
    assert(nV_out);

    if (!fgets(buf, 256, file)) {
        goto bad_file;
    }
    if (sscanf(buf, "%u", &nV) != 1) {
        goto bad_file;
    }
    printf("%s(): Getting %u vertexes\n", __func__, nV);

    vertex = malloc(nV * sizeof(double) * 3);
    if (!vertex) {
        printf("Error: %s(): failed to allocate memory for vertexes\n",
                __func__);
        return NULL;
    }

     for (i = 0; i < nV; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_malloc;
        }
        if (sscanf(buf, "%lf %lf %lf", &x, &y, &z) != 3) {
            goto out_malloc;
        }
        vertex[3 * i + 0] = x;
        vertex[3 * i + 1] = y;
        vertex[3 * i + 2] = z;
    }

    *nV_out = nV;

    return vertex;

out_malloc:
    free(vertex);

bad_file:
    printf("Error: %s(): bad input file format\n", __func__);

    return NULL;
}

unsigned int *get_coord_tetras_from_ani3d_out(FILE *file, unsigned int *nT_out,
        unsigned int **tetra_material)
{
    char buf[256];
    unsigned int nT, i;
    unsigned int *tetra = NULL, *material = NULL;
    unsigned int v0, v1, v2, v3, m;

    assert(file);
    assert(nT_out);

    if (!fgets(buf, 256, file)) {
        goto bad_file;
    }

    if (sscanf(buf, "%u", &nT) != 1) {
        goto bad_file;
    }

    printf("%s(): Getting %u tetras\n", __func__, nT);

    tetra = malloc(nT * sizeof(unsigned int) * 4);
    if (!tetra) {
        printf("Error: %s(): failed to allocate memory for tetras\n", __func__);
        return NULL;
    }

    if (tetra_material) {
        material = malloc(nT * sizeof(unsigned int));
        if (!material) {
            free(tetra);
            printf( "Error: %s(): failed to allocate memory for tetras material\n",
                    __func__);
            return NULL;
        }
    }

    for (i = 0; i < nT; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_malloc;
        }
        if (sscanf(buf, "%u %u %u %u %u", &v0, &v1, &v2, &v3, &m) != 5) {
            goto out_malloc;
        }
        tetra[4 * i + 0] = v0;
        tetra[4 * i + 1] = v1;
        tetra[4 * i + 2] = v2;
        tetra[4 * i + 3] = v3;
        if (tetra_material) {
            material[i] = m;
        }
    }

    if (tetra_material) {
        *tetra_material = material;
    }

    *nT_out = nT;

    return tetra;

out_malloc:
    free(tetra);
    free(material);

bad_file:
    printf("Error: %s(): bad input file format\n", __func__);

    return NULL;
}

unsigned int *get_coord_faces_from_gmsh_msh(FILE *file, unsigned int *nF_out)
{
    char buf[256];
    unsigned int v0, v1, v2;
    unsigned int t_1, t_2, t_3, t_4, t_5;
    unsigned int i;
    unsigned int n_elements = 0, nF = 0;
    unsigned int *face = NULL;
    const unsigned int triangle = 2;
    const unsigned int quadrangle = 3;

    assert(file);
    assert(nF_out);

    for (i = 0; i < 3; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_error;
        }
    }
    if (sscanf(buf, "%u", &n_elements) != 1) {
        goto out_error;
    }

    for (i = 0; i < n_elements; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_error;
        }

        sscanf(buf, "%u %u", &t_1, &t_2);

        if (t_2 == triangle) {
            break;
        }
    }

    nF = n_elements - t_1 + 1;
    printf("%s(): Getting %u faces\n", __func__, nF);
    face = malloc(nF * sizeof(unsigned int) * 3);
    if (!face) {
        printf( "Error: %s(): failed to allocate memory for face\n",
                __func__);
        return NULL;
    }

    i = 0;
    do {
        if (sscanf(buf, "%u %u %u %u %u %u %u %u", &t_1, &t_2,
                &t_3, &t_4, &t_5, &v0, &v1, &v2) != 8) {
            goto out_error;
        }
        if (t_2 == triangle) {
            face[3 * i + 0] = v0;
            face[3 * i + 1] = v1;
            face[3 * i + 2] = v2;
        } else if (t_2 == quadrangle) {
            printf("Error: %s(): selected 3D mesh\n", __func__);
            goto out_error;
        }
        
        i++;
        if (i < nF) {
            if (!fgets(buf, 256, file)) {
                goto out_error;
            }
        } else {
            break;
        }
    } while(1);

    *nF_out = nF;

    return face;

out_error:
    if (face) {
        free(face);
    }

    printf("Error: %s(): bad input file format\n", __func__);

    return NULL;
}

unsigned int *get_coord_faces_from_ani3d_out(FILE *file, unsigned int *nF_out,
        unsigned int **face_material)
{
    char buf[256];
    unsigned int nF = 0, i;
    unsigned int *face = NULL, *material = NULL;
    unsigned int v0, v1, v2, m;

    assert(file);
    assert(nF_out);

    if (!fgets(buf, 256, file)) {
        goto bad_file;
    }
    if (sscanf(buf, "%u", &nF) != 1) {
        goto bad_file;
    }
    printf("%s(): Getting %u faces\n", __func__, nF);

    face = malloc(nF * sizeof(unsigned int) * 3);
    if (!face) {
        printf("Error: %s(): failed to allocate memory for face\n",
            __func__);
        return NULL;
    }

    for (i = 0; i < nF; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_malloc;
        }
        if (sscanf(buf, "%u %u %u %u", &v0, &v1, &v2, &m) != 4) {
            goto out_malloc;
        }
        face[3 * i + 0] = v0;
        face[3 * i + 1] = v1;
        face[3 * i + 2] = v2;
    }

    *nF_out = nF;

    if (face_material) {
        *face_material = material;
    }

    return face;

out_malloc:
    free(face);
    free(material);

bad_file:
    printf("Error: %s(): bad input file format\n", __func__);

    return NULL;
}

struct vector_simple calc_coordvector(struct vertex_simple *v_start,
        struct vertex_simple *v_end)
{
    struct vector_simple coordvector;
    coordvector.x = v_end->x - v_start->x;
    coordvector.y = v_end->y - v_start->y;
    coordvector.z = v_end->z - v_start->z;
    return coordvector;
}

struct vertex_simple calc_coordcenter(struct vertex_simple *v1,
        struct vertex_simple *v2)
{
    struct vertex_simple coordcenter;
    coordcenter.x = (v2->x + v1->x) / 2.0;
    coordcenter.y = (v2->y + v1->y) / 2.0;
    coordcenter.z = (v2->z + v1->z) / 2.0;
    return coordcenter;
}

struct vertex_simple calc_coord_intersection_median(struct vertex_simple *v1,
        struct vertex_simple *v2, struct vertex_simple *v3)
{
    struct vertex_simple coormedian;
    coormedian.x = (v1->x + v2->x + v3->x) / 3.0;
    coormedian.y = (v1->y + v2->y + v3->y) / 3.0;
    coormedian.z = (v1->z + v2->z + v3->z) / 3.0;
    return coormedian;
}

long double calc_vectorlength(struct vector_simple *v1)
{
    long double vectorlength;
    vectorlength = sqrtl(powl(v1->x, 2) + powl(v1->y, 2) + powl(v1->z, 2));
    return vectorlength;
}

struct vector_simple calc_vector_product(struct vector_simple *v1,
        struct vector_simple *v2)
{
    struct vector_simple coord_vector_product;
    coord_vector_product.x = (v1->y * v2->z - v1->z * v2->y);
    coord_vector_product.y = (v1->z * v2->x - v1->x * v2->z);
    coord_vector_product.z = (v1->x * v2->y - v1->y * v2->x);
    return coord_vector_product;
}

struct vertex_simple read_coordvertex(double *vertex, unsigned int k)
{
    struct vertex_simple v;
    v.x = (long double) vertex[k * 3 + 0];
    v.y = (long double) vertex[k * 3 + 1];
    v.z = (long double) vertex[k * 3 + 2];
    return v;
}

long double calc_scalar_product(struct vector_simple *v1, struct vector_simple *v2)
{
    long double scalar_product;
    scalar_product = (long double) (v1->x * v2->x + v1->y * v2->y
            + v1->z * v2->z);
    return scalar_product;
}

struct adjacent_faces *get_vertex_belong(unsigned int *face, unsigned int nV_in,
        unsigned int nF_in, unsigned int **valid_v, unsigned int *nV_out)
{
    struct adjacent_faces *vertex_belong = NULL;
    unsigned int i, j;
    unsigned int nS = 0;
    unsigned int n = 0;
    unsigned int *valid = NULL;
    unsigned int cnt = 0;

    assert(face);
    assert(nV_in);
    assert(nF_in);

    vertex_belong = calloc(nV_in, sizeof(struct adjacent_faces));
    valid = calloc(nV_in, sizeof(unsigned int));
    for (i = 0; i < nF_in; i++) {
        for (j = 0; j < 3; j++) {
            n = face[i * 3 + j] - 1;
            nS = vertex_belong[n].cnt;
            assert(nS < MAX_ADJACENT_FACES);
            vertex_belong[n].faces[nS] = i;
            vertex_belong[n].cnt++;
        }
    }
    for (i = 0; i < nV_in; i++) {
        if (vertex_belong[i].cnt != 0) {
            cnt++;
            valid[i] = 1;
        }
    }
    *valid_v = valid;
    *nV_out = cnt;
    printf("%s() read successfully\n", __func__);
    return vertex_belong;
}

long double calc_area_of_a_triangle(struct vector_simple *rjk0,
        struct vector_simple *rjk1, struct vector_simple *rk0_k1)
{
    long double perimeter = 0, area_triangle = 0;
    long double a, b, c;
    a = calc_vectorlength(rjk0);
    b = calc_vectorlength(rjk1);
    c = calc_vectorlength(rk0_k1);
    perimeter = (a + b + c) / 2;
    area_triangle = sqrtl(perimeter * (perimeter - a) * (perimeter - b) * (perimeter - c));
    return area_triangle;
}

void calc_ljk_and_cosnjk(struct vertex_simple *v1, struct vertex_simple *v2,
        struct vertex_simple *v3, long double *cosnjk_x, long double *cosnjk_y,
        long double *cosnjk_z, long double *l_ljk, struct vertex_simple *tj,
        long double *area)
{
    struct vertex_simple qjk[2], qjm, tjk[2];
    struct vector_simple rjk[2], rk0_k1, lj[2];
    struct vector_simple nj1, njk[2];
    long double length_njk[2], l_lj[2];
    unsigned int i;
    long double cosn_x[2], cosn_y[2], cosn_z[2];
    long double area_k;

    qjk[0] = calc_coordcenter(v1, v3);
    qjk[1] = calc_coordcenter(v2, v3);
    qjm = calc_coord_intersection_median(v1, v2, v3);
    rjk[0] = calc_coordvector(v3, v1);
    rjk[1] = calc_coordvector(v3, v2);
    rk0_k1 = calc_coordvector(v1, v2);
    lj[0] = calc_coordvector(&qjk[0], &qjm);
    lj[1] = calc_coordvector(&qjm, &qjk[1]);
    for (i = 0; i < 2; i++) {
        l_lj[i] = calc_vectorlength(&lj[i]);
        tjk[i] = calc_coordcenter(&qjk[i], &qjm);
    }
    nj1 = calc_vector_product(&rjk[0], &rjk[1]);

    for (i = 0; i < 2; i++) {
        njk[i] = calc_vector_product(&lj[i], &nj1);
    }

    for (i = 0; i < 2; i++) {
        length_njk[i] = calc_vectorlength(&njk[i]);
        if (fabsl(length_njk[i]) < 0.0000000001) {
            cosn_x[i] = 0;
            cosn_y[i] = 0;
            cosn_z[i] = 0;
        } else {
            cosn_x[i] = njk[i].x / length_njk[i];
            cosn_y[i] = njk[i].y / length_njk[i];
            cosn_z[i] = njk[i].z / length_njk[i];
        }
    }

    for (i = 0; i < 2; i++) {
        cosnjk_x[i] = cosn_x[i];
        cosnjk_y[i] = cosn_y[i];
        cosnjk_z[i] = cosn_z[i];
        l_ljk[i] = l_lj[i];
        tj[i] = tjk[i];
    }

    area_k = calc_area_of_a_triangle(&rjk[0], &rjk[1], &rk0_k1);
    *area = area_k;
}

void *calculation_A(void *_arg)
{
    unsigned int i, k, j, v_cnt, c, step;
    unsigned int n, v, f;
    unsigned int nV_start = 0;
    unsigned int nV_end = 0;
    struct vertex_simple w, v1, v2, v3;
    long double influence_face, a;
    long double area, area_k;
    long double cos_nx[2], cos_ny[2], cos_nz[2];
    long double l_rjk[2];
    struct vertex_simple tj[2];
    struct vector_simple distance;
    struct for_calculate_A *arg = (struct for_calculate_A *)_arg;

    step = arg->nV / arg->divider;
    if (arg->cnt == 1) {
        nV_start = 0;
        nV_end = step;
    } else if (arg->cnt == 2) {
        nV_start = step;
        nV_end = step * 2;
    } else if (arg->cnt == 3) {
        nV_start = step * 2;
        nV_end = step * 3;
    } else if (arg->cnt == 4) {
        nV_start = step * 3;
        nV_end = arg->nV;
    } else {
        printf("Error: %s() wrong input argument - %d\n", __func__, arg->cnt);
        return (void *)-1;
    }

    for (k = nV_start; k < nV_end; k++) {
        if (arg->vertexes_belong_to_faces[k].cnt != 0) {
            w = read_coordvertex(arg->vertex, k);
            for (i = 0; i < arg->nV; i++) {
                n = arg->vertexes_belong_to_faces[i].cnt;
                if (n != 0) {
                    v3 = read_coordvertex(arg->vertex, i);
                    influence_face = 0;
                    area = 0;
                    for (j = 0; j < n; j++) {
                        f = arg->vertexes_belong_to_faces[i].faces[j];
                        v_cnt = 0;
                        for (c = 0; c < 3; c++) {
                            v = arg->face[f * 3 + c] - 1;
                            if (v != i) {
                                if (v_cnt == 0) {
                                    v1 = read_coordvertex(arg->vertex, v);
                                    v_cnt = 1;
                                } else {
                                    v2 = read_coordvertex(arg->vertex, v);
                                }
                            }
                        }
                        calc_ljk_and_cosnjk(&v1, &v2, &v3, cos_nx, cos_ny,
                                cos_nz, l_rjk, tj, &area_k);
                        area = area + area_k;
                        a = 0;
                        for (c = 0; c < 2; c++) {
                            distance.x = w.x - tj[c].x;
                            distance.y = w.y - tj[c].y;
                            distance.z = w.z - tj[c].z;
                            a = a + arg->perm * l_rjk[c] * (distance.x * cos_nx[c]
                                    + distance.y * cos_ny[c]  + distance.z * cos_nz[c])
                                    / powl((powl(distance.x, 2.0) + powl(distance.y,2.0)
                                    + powl(distance.z, 2.0)), 3.0 / 2.0);
                        }
                        influence_face = influence_face + a;
                    }
                    arg->A[k * arg->nV + i] = influence_face/ (M_PI * -4.0 * area);
                }
            }
        }
    }

    return (void *)0;
}

int pthreads_for_A(unsigned int *face, unsigned int nV, double *vertex,
        struct adjacent_faces *vertexes_belong_to_faces, long double *A,
        long double perm)
{
    static struct for_calculate_A arg1, arg2, arg3, arg4;
    unsigned int divide;
    pthread_t pthread_1;
    pthread_t pthread_2;
    pthread_t pthread_3;
    pthread_t pthread_4;
    int res;

    assert(face);
    assert(vertexes_belong_to_faces);
    assert(vertex);
    assert(A);

    divide = 4;
    arg1.face = face;
    arg1.cnt = 1;
    arg1.divider = divide;
    arg1.nV = nV;
    arg1.vertex = vertex;
    arg1.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg1.A = A;
    arg1.perm = perm;

    arg2.face = face;
    arg2.cnt = 2;
    arg2.divider = divide;
    arg2.nV = nV;
    arg2.vertex = vertex;
    arg2.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg2.A = A;
    arg2.perm = perm;

    arg3.face = face;
    arg3.cnt = 3;
    arg3.divider = divide;
    arg3.nV = nV;
    arg3.vertex = vertex;
    arg3.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg3.A = A;
    arg3.perm = perm;

    arg4.face = face;
    arg4.cnt = 4;
    arg4.divider = divide;
    arg4.nV = nV;
    arg4.vertex = vertex;
    arg4.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg4.A = A;
    arg4.perm = perm;

    res = pthread_create(&pthread_1, NULL, &calculation_A, &arg1);
    if (res != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_create(&pthread_2, NULL, &calculation_A, &arg2) != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_create(&pthread_3, NULL, &calculation_A, &arg3) != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_create(&pthread_4, NULL, &calculation_A, &arg4) != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_join(pthread_1, NULL) != 0) {
        fprintf(stderr, "pthread_1_join error\n");
        return -1;
    }
    if (pthread_join(pthread_2, NULL) != 0) {
        fprintf(stderr, "pthread_1_join error\n");
        return -1;
    }
    if (pthread_join(pthread_3, NULL) != 0) {
        fprintf(stderr, "pthread_1_join error\n");
        return -1;
    }
    if (pthread_join(pthread_4, NULL) != 0) {
        fprintf(stderr, "pthread_1_join error\n");
        return -1;
    }
    return 0;
}

static void *calculation_C(void *_arg)
{
    struct vertex_simple v1, v2, v3;
    unsigned int i, j, c;
    unsigned int n, f, v;
    unsigned int v12_cnt;
    long double C, influence;
    long double l_rjk[2];
    struct vertex_simple tj[2];
    long double cos_nx[2], cos_ny[2], cos_nz[2];
    long double area_k, area;
    unsigned int step, nV_start, nV_end;

    struct for_calculate_C *arg = (struct for_calculate_C *)_arg;

    step = arg->nV / arg->divider;
    if (arg->cnt == 1) {
        nV_start = 0;
        nV_end = step;
    } else if (arg->cnt == 2) {
        nV_start = step;
        nV_end = arg->nV;
    } else {
        printf("Error: Wrong thread argument arg->cnt = %u\n", arg->cnt);
        return (void *)-1;
    }

    for (i = nV_start; i < nV_end; i++) {
        n = arg->vertexes_belong_to_faces[i].cnt;
        if (n != 0) {
            v3 = read_coordvertex(arg->vertex, i);
            influence = 0;
            area = 0;
            for (j = 0; j < n; j++) {
                f = arg->vertexes_belong_to_faces[i].faces[j];
                v12_cnt = 0;
                for (c = 0; c < 3; c++) {
                    v = arg->face[f * 3 + c] - 1;
                    if (v != i) {
                        if (v12_cnt == 0) {
                            v1 = read_coordvertex(arg->vertex, v);
                            v12_cnt = 1;
                        } else {
                            v2 = read_coordvertex(arg->vertex, v);
                        }
                    }
                }
                calc_ljk_and_cosnjk(&v1, &v2, &v3, cos_nx, cos_ny, cos_nz,
                        l_rjk, tj, &area_k);
                C = 0;
                for (c = 0; c < 2; c++) {
                    C = C + arg->perm * l_rjk[c] * (arg->H->x * cos_nx[c]
                            + arg->H->y * cos_ny[c] + arg->H->z * cos_nz[c]);
                }
                influence = influence + C;
                area = area + area_k;
            }
            arg->C[i] = influence / area;
        }
    }
    return (void *)0;
}

int pthreads_for_C(unsigned int *face, unsigned int nV, double *vertex,
        struct adjacent_faces *vertexes_belong_to_faces,
        struct vertex_simple *H, long double *C, long double perm)
{
    static struct for_calculate_C arg1, arg2;
    unsigned int divide;
    pthread_t pthread_1;
    pthread_t pthread_2;
    void *thread_ret;

    assert(face);
    assert(vertexes_belong_to_faces);
    assert(vertex);
    assert(C);
    assert(H);

    divide = 4;
    arg1.face = face;
    arg1.cnt = 1;
    arg1.divider = divide;
    arg1.nV = nV;
    arg1.vertex = vertex;
    arg1.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg1.C = C;
    arg1.H = H;
    arg1.perm = perm;

    arg2.face = face;
    arg2.cnt = 2;
    arg2.divider = divide;
    arg2.nV = nV;
    arg2.vertex = vertex;
    arg2.vertexes_belong_to_faces = vertexes_belong_to_faces;
    arg2.C = C;
    arg2.H = H;
    arg2.perm = perm;

    if (pthread_create(&pthread_1, NULL, &calculation_C, &arg1) != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_create(&pthread_2, NULL, &calculation_C, &arg2) != 0) {
        fprintf(stderr, "pthread_create failed\n");
        return -1;
    }
    if (pthread_join(pthread_1, &thread_ret) != 0) {
        fprintf(stderr, "pthread_1_join error\n");
        return -1;
    }

    if (thread_ret) {
        printf("Error: Thread 1 failed - %ld\n", (long) thread_ret);
        return -1;
    }

    if (pthread_join(pthread_2, &thread_ret) != 0) {
        fprintf(stderr, "pthread_2_join error\n");
        return -1;
    }

    if (thread_ret) {
        printf("Error: Thread 2 failed - %ld\n", (long) thread_ret);
        return -1;
    }

    return 0;
}

long double *get_C(unsigned int *face, unsigned int nV_in, double *vertex,
        struct adjacent_faces *surfaces, struct vertex_simple *H,
        unsigned int *valid_mask, unsigned int nV_out)
{
    long double *interaction_C = NULL, *C_valid = NULL;
    unsigned int nV;
    struct vertex_simple v1, v2, v3;
    unsigned int i, j, c;
    unsigned int n, f, v;
    unsigned int v12_cnt, cnt;
    long double C, influence;
    long double l_rjk[2];
    struct vertex_simple tj[2];
    long double cos_nx[2], cos_ny[2], cos_nz[2];
    long double area_k, area;

    assert(face);
    assert(surfaces);
    assert(vertex);

    nV = nV_in;
    interaction_C = malloc(sizeof(long double) * nV);
    if (!interaction_C) {
        printf("Error: %s(): failed to allocate memory for interaction_A\n",
                __func__);
        return NULL;
    }
    C_valid = malloc(sizeof(long double) * nV_out);
    if (!C_valid) {
        free(interaction_C);
        printf("Error: %s(): failed to allocate memory for interaction_A\n",
                __func__);
        return NULL;
    }

    for (i = 0; i < nV; i++) {
        n = surfaces[i].cnt;
        if (n != 0) {
            v3 = read_coordvertex(vertex, i);
            influence = 0;
            area = 0;
            for (j = 0; j < n; j++) {
                f = surfaces[i].faces[j];
                v12_cnt = 0;
                for (c = 0; c < 3; c++) {
                    v = face[f * 3 + c] - 1;
                    if (v != i) {
                        if (v12_cnt == 0) {
                            v1 = read_coordvertex(vertex, v);
                            v12_cnt = 1;
                        } else {
                            v2 = read_coordvertex(vertex, v);
                        }
                    }
                }
                calc_ljk_and_cosnjk(&v1, &v2, &v3, cos_nx, cos_ny, cos_nz,
                        l_rjk, tj, &area_k);
                C = 0;
                for (c = 0; c < 2; c++) {
                    C = C  + 0.15 * l_rjk[c] * (H->x * cos_nx[c] + H->y * cos_ny[c]
                            + H->z * cos_nz[c]);
                }
                influence = influence + C;
                area = area + area_k;
            }
            interaction_C[i] = influence / area;
        }
    }

    cnt = 0;
    for (i = 0; i < nV; i++) {
        if (valid_mask[i] == 1) {
            C_valid[cnt] = interaction_C[i];
            cnt++;
        }
    }

    return C_valid;
}

gsl_vector *get_X(long double *A, long double *C, unsigned int nV_in)
{
    gsl_vector *interaction_X = NULL;
    unsigned int nV;
    gsl_matrix *U = NULL;
    gsl_matrix *V = NULL;
    gsl_vector *S = NULL;
    gsl_vector *work = NULL;
    unsigned int i, j;
    gsl_vector *b = NULL;
    gsl_matrix *TEST_A = NULL;
    gsl_vector *test_b = NULL;
    int ret;
    double temp = 0;

    assert(A);
    assert(C);
    nV = nV_in;
    interaction_X = gsl_vector_alloc(nV);
    if (!interaction_X) {
        printf("Error: %s(): failed to allocate memory for interaction_X\n", __func__);
        return NULL;
    }
    U = gsl_matrix_alloc((nV + 1), nV);
    if (!U) {
        printf("Error: %s(): failed to allocate memory for U\n", __func__);
        return NULL;
    }
    V = gsl_matrix_alloc(nV, nV);
    if (!V) {
        printf("Error: %s(): failed to allocate memory for V\n", __func__);
        return NULL;
    }
    S = gsl_vector_alloc(nV);
    if (!S) {
        printf("Error: %s(): failed to allocate memory for S\n", __func__);
        return NULL;
    }
    work = gsl_vector_alloc(nV);
    if (!work) {
        printf("Error: %s(): failed to allocate memory for work\n", __func__);
        return NULL;
    }
    TEST_A = gsl_matrix_alloc((nV), nV);
    if (!TEST_A) {
        printf("Error: %s(): failed to allocate memory for U\n", __func__);
        return NULL;
    }

    for (i = 0; i < nV; i++) {
        for (j = 0; j < nV; j++) {
            gsl_matrix_set(U, i, j, (double) A[i * nV + j]);
            gsl_matrix_set(TEST_A, i, j, (double) A[i * nV + j]);
        }
    }

    for (i = 0; i < nV; i++) {
        gsl_matrix_set(U, nV, i, 1.0);
    }

    ret = gsl_linalg_SV_decomp(U, V, S, work);
    printf("gsl_linalg_SV_decomp ret = %d\n", ret);

    b = gsl_vector_alloc(nV + 1);
    if (!b) {
        printf("Error: %s(): failed to allocate memory for b\n", __func__);
        return NULL;
    }

    test_b = gsl_vector_alloc(nV);
    if (!test_b) {
        printf("Error: %s(): failed to allocate memory for b\n", __func__);
        return NULL;
    }

    for (i = 0; i < nV; i++) {
        gsl_vector_set(b, i, (double) C[i]);
    }

    gsl_vector_set(b, nV, 0.0);

    gsl_linalg_SV_solve(U, V, S, b, interaction_X);

    printf("examination Ax=\n");
    for (i = 0; i < nV; i++) {
        for (j = 0; j < nV; j++) {
            temp = temp + gsl_vector_get(interaction_X, i)
                    * gsl_matrix_get(TEST_A, i, j);
        }
        gsl_vector_set(test_b, i, temp);
        temp = 0;
    }

    gsl_vector_free(S);
    gsl_vector_free(work);
    gsl_matrix_free(U);
    gsl_matrix_free(V);
    gsl_vector_free(b);

    gsl_matrix_free(TEST_A);
    gsl_vector_free(test_b);

    return interaction_X;
}

static char *get_app_path(char *path_to_app)
{
    char *path;

    path = strdup(path_to_app);
    if (!path) {
        printf("Error: strdup() failed - %d\n", errno);
        return NULL;
    }

    path = dirname(path);

    return path;
}

int main(int argc, char **argv)
{
    char str[256];
    char *path;
    int flag_input_format = 0;
    const char *short_options = "i:t:p:x:y:z:e:h:m:a:b:c:l:w:N:n:A:g:r:u:";
    int format_txt = 0;
    int format_pos = 0;
    int flag_ambient_field = 0;
    char *path_mesh_file = NULL;
    int rez;
    int option_index = -1;
    int ret = 0;
    FILE *mesh_file = NULL;
    unsigned int nV = 0, nV_valid;
    double *vertex = NULL;
    unsigned int nT = 0;
    unsigned int *tetra = NULL, *tetra_material = NULL;
    unsigned int nF = 0;
    unsigned int *face = NULL, *face_material = NULL;
    struct adjacent_faces *vertexes_belong_to_faces = NULL;
    unsigned int i = 0;
    unsigned int j;
    unsigned int cnt_1, cnt_2;
    unsigned int *valid_vertex = NULL;
    long double *X_out = NULL;
    struct vertex_simple H, temp;
    gsl_vector *X = NULL;
    long double inspection = 0;
    long double permeability = 100, thickness = 0.1;
    struct vertex_simple position_plane;
    long double size_length_plane = 0.0, size_width_plane = 0.0,
            position_line_width_plane = 0.0;
    unsigned int nPoint_l_plane = 0, nPoint_w_plane = 0, nPoint_graph = 0, graph_result_type = 0;
    char *Axis_plane = NULL;

    position_plane.x = 0.0;
    position_plane.y = 0.0;
    position_plane.z = 0.0;

    path = get_app_path(argv[0]);
    if (!path) {
        return 1;
    }

    const struct option long_options[] = {
            { "input_format", optional_argument, NULL, 'i' },
            { "output_format_txt", optional_argument, NULL, 't' },
            { "output_format_pos", optional_argument, NULL, 'p' },
            { "Hx", optional_argument, NULL, 'x' },
            { "Hy", optional_argument, NULL, 'y' },
            { "Hz", optional_argument, NULL, 'z' },
            { "permeability", optional_argument, NULL, 'e' },
            { "thickness", optional_argument, NULL, 'h' },
            { "ambient_field", optional_argument, NULL, 'm' },
            { "position_x", optional_argument, NULL, 'a' },
            { "position_y", optional_argument, NULL, 'b' },
            { "position_z", optional_argument, NULL, 'c' },
            { "size_length", optional_argument, NULL, 'l' },
            { "size_width", optional_argument, NULL, 'w' },
            { "nPoint_l", optional_argument, NULL, 'N' },
            { "nPoint_w", optional_argument, NULL, 'n' },
            { "position_graph_width", optional_argument, NULL, 'g' },
            { "nPoint_graph", optional_argument, NULL, 'r' },
            { "Axis", optional_argument, NULL, 'A' },
            { "graph_result_type", optional_argument, NULL, 'u' },
            { NULL, 0, NULL, 0 } };

    while ((rez = getopt_long(argc, argv, short_options, long_options, &option_index)) != -1) {
        switch (rez) {
        case 'i':
            if (optarg != NULL) {
                flag_input_format = atoi(optarg);
                if (flag_input_format == FORMAT_TYPE_GMSH_MSH ||
                        flag_input_format == FORMAT_TYPE_ANI3D_OUT) {
                    printf("flag_input_format = %d\n", flag_input_format);
                } else {
                    printf("Enter the correct value 0(input_file.out) | 1(input_file.msh)\n");
                }
            } else {
                printf("error!\n");
            }
            break;
        case 't':
            if (optarg != NULL) {
                format_txt = atoi(optarg);
                printf("format_txt = %d\n", format_txt);
            } else {
                printf("error!\n");
            }
            break;
        case 'p':
            if (optarg != NULL) {
                format_pos = atoi(optarg);
                printf("format_pos = %d\n", format_pos);
            } else {
                printf("error!\n");
            }
            break;
        case 'x':
            if (optarg != NULL) {
                H.x = (long double) atof(optarg);
                printf("Hx = %LF\n", H.x);
            } else {
                printf("error!\n");
            }
            break;
        case 'y':
            if (optarg != NULL) {
                H.y = (long double) atof(optarg);
                printf("Hy = %LF\n", H.y);
            } else {
                printf("error!\n");
            }
            break;
        case 'z':
            if (optarg != NULL) {
                H.z = (long double) atof(optarg);
                printf("Hz = %LF\n", H.z);
            } else {
                printf("error!\n");
            }
            break;
        case 'e':
            if (optarg != NULL) {
                permeability = (long double) atof(optarg);
                printf("permeability = %LF\n", permeability);
            } else {
                printf("error!\n");
            }
            break;
        case 'h':
            if (optarg != NULL) {
                thickness = (long double) atof(optarg);
                printf("thickness = %LF\n", thickness);
            } else {
                printf("error!\n");
            }
            break;
        case 'm':
            if (optarg != NULL) {
                flag_ambient_field = atoi(optarg);
                if (flag_ambient_field == 0 || flag_ambient_field == 1) {
                    printf("flag_ambient_field = %d\n", flag_ambient_field);
                } else {
                    printf("Enter the correct value 0(input_file.out) | 1"
                            "(input_file.msh)\n");
                }
            } else {
                printf("error!\n");
            }
            break;
        case 'a':
            if (optarg != NULL) {
                position_plane.x = (long double) atof(optarg);
                printf("starting position x (plane) = %LF\n", position_plane.x);
            } else {
                printf("error!\n");
            }
            break;
        case 'b':
            if (optarg != NULL) {
                position_plane.y = (long double) atof(optarg);
                printf("starting position y (plane) = %LF\n", position_plane.y);
            } else {
                printf("error!\n");
            }
            break;
        case 'c':
            if (optarg != NULL) {
                position_plane.z = (long double) atof(optarg);
                printf("starting position z (plane) = %LF\n", position_plane.z);
            } else {
                printf("error!\n");
            }
            break;
        case 'l':
            if (optarg != NULL) {
                size_length_plane = (long double) atof(optarg);
                printf("size length of plane = %LF\n", size_length_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'w':
            if (optarg != NULL) {
                size_width_plane = (long double) atof(optarg);
                printf("size width of plane  = %LF\n", size_width_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'N':
            if (optarg != NULL) {
                nPoint_l_plane = (long double) atof(optarg);
                printf("nPoint_x = %d\n", nPoint_l_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'n':
            if (optarg != NULL) {
                nPoint_w_plane = (long double) atof(optarg);
                printf("nPoint_y = %d\n", nPoint_w_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'A':
            if (optarg != NULL) {
                Axis_plane = optarg;
                printf("Axis = %s\n", Axis_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'g':
            if (optarg != NULL) {
                position_line_width_plane = (long double) atof(optarg);
                printf("position_line_width = %LF\n",
                        position_line_width_plane);
            } else {
                printf("error!\n");
            }
            break;
        case 'r':
            if (optarg != NULL) {
                nPoint_graph = (long double) atof(optarg);
                printf("nPoint_graph = %d\n", nPoint_graph);
            } else {
                printf("error!\n");
            }
            break;
        case 'u':
            if (optarg != NULL) {
                graph_result_type = atoi(optarg);
                if (graph_result_type == 0) {
                    printf("Output result intensity\n");
                } else if (graph_result_type == 1) {
                    printf("Output result potential\n");
                } else if (graph_result_type == 2) {
                    printf("Output result induction\n");
                } else {
                    printf("Error: graph_result_type - %d\n",
                            graph_result_type);
                    return -1;
                }
            } else {
                printf("error!\n");
            }
            break;
        case '?':
        default:
            printf("found unknown option\n");
            break;
        }
    }

    if (argc != 22) {
        printf("Error: Invalid number of arguments\n");
        usage();
        printf("-i or --input_format 0(input_file.out) | 1(input_file.msh)\n");
        printf("-t or --output_format_txt if 1 output the result in a format txt\n");
        printf("-p or --output_format_pos if 1 output the result in a format msh.pos\n");
        printf("-x or --Hx value ambient_field_x\n");
        printf("-y or --Hy value ambient_field_y\n");
        printf("-z or --Hz value ambient_field_z\n");
        printf("-e or --permeability value permeability\n");
        printf("-h or --thickness\n");
        printf("-m or --flag_ambient_field 0(print the result without ambient_field)"
        " | 1(print the result with ambient_field)\n");
        printf("-a or --position_plane_x\n");
        printf("-b or --position_plane_y\n");
        printf("-c or --position_plane_z\n");
        printf("-l or --size_length_plane\n");
        printf("-w or --size_width_plane\n");
        printf("-N or --nPoint_x_plane\n");
        printf("-n or --nPoint_y_plane\n");
        printf("-A or --Axis\n");
        printf("-g or posirion_line_width_plane\n");
        printf("-u or --graph_result_type\n");
        printf("-r or --nPoint_graph\n");
        printf("Enter the filename\n");
        return -1;
    } else {
        path_mesh_file = argv[21];
        printf("mesh_filename = %s\n", path_mesh_file);
    }

    FILE *file;
    char *fname;
    char *basec;
    basec = strdup(path_mesh_file);
    char *tmp;
    tmp = basename(basec);
    while (tmp[i] != '.') {
        basec[i] = tmp[i];
        i++;
    }
    basec[i] = '\0';
    unsigned int len;
    len = strlen(basec);
    len += 12;
    fname = malloc(len);
    sprintf(fname, "result_%s.txt", basec);
    printf("fname = %s\n", fname);
    file = fopen(fname, "w");
    if (file == NULL) {
        printf("Error opening file '%s'", fname);
        return 1;
    }

    permeability = permeability * thickness;
    printf("permeability_recounted = %LF\n", permeability);
    printf("Machine info:\n");
    printf("\t sizeof(double) = %lu\n", sizeof(double));
    printf("\t sizeof(long double) = %lu\n", sizeof(long double));
    printf("\t DBL_MIN = %g\n", DBL_MIN);
    printf("\t DBL_MAX = %g\n", DBL_MAX);
    printf("\t LDBL_MIN = %Lg\n", LDBL_MIN);
    printf("\t LDBL_MAX = %Lg\n", LDBL_MAX);
    printf("\n");
    printf("Working with '%s' file\n", path_mesh_file);

    mesh_file = fopen(path_mesh_file, "r");
    if (!mesh_file) {
        printf("Error: Can't open '%s' file - %d (%s)\n", path_mesh_file, errno, strerror(errno));
        return 1;
    }

    if (flag_input_format == FORMAT_TYPE_GMSH_MSH) {
        vertex = get_coord_vertexes_from_gmsh_msh(mesh_file, &nV);
        if (!vertex) {
            printf("Error: Can't get vertexes\n");
            ret = -1;
            goto out_get;
        }

        face = get_coord_faces_from_gmsh_msh(mesh_file, &nF);
        if (!face) {
            printf("Error: Can't get faces\n");
            ret = -1;
            goto out_get;
        }
    } else if (flag_input_format == FORMAT_TYPE_ANI3D_OUT) {
        vertex = get_coord_vertexes_from_ani3d_out(mesh_file, &nV);
        if (!vertex) {
            printf("Error: Can't get vertexes\n");
            ret = -1;
            goto out_get;
        }

        tetra = get_coord_tetras_from_ani3d_out(mesh_file, &nT, &tetra_material);
        if (!tetra) {
            printf("Error: Can't get tetras\n");
            ret = -1;
            goto out_get;
        }

        face = get_coord_faces_from_ani3d_out(mesh_file, &nF, &face_material);
        if (!face) {
            printf("Error: Can't get faces\n");
            ret = -1;
            goto out_get;
        }
    } else {
        printf("Error: %s(): wrong input_format - %d\n", __func__,
                flag_input_format);
        ret = -1;
        goto out_get;
    }

    fclose(mesh_file);
    mesh_file = NULL;
    printf("Data from '%s' read successfully\n", path_mesh_file);

    vertexes_belong_to_faces = get_vertex_belong(face, nV, nF, &valid_vertex,
            &nV_valid);
    if (!vertexes_belong_to_faces) {
        printf("Error: Can't get vertex belong");
        ret = 1;
        goto out_get;
    }
    printf("nV_valid = %u\n", nV_valid);

    A = malloc(nV * sizeof(long double) * nV);
    if (!A) {
        printf("Error: Can't get A\n");
        ret = 1;
        goto out_get;
    }

    A_valid = malloc(nV_valid * sizeof(long double) * nV_valid);
    if (!A_valid) {
        printf("Error: Can't get A_valid\n");
        ret = 1;
        goto out_get;
    }

    if (pthreads_for_A(face, nV, vertex, vertexes_belong_to_faces, A,
            permeability) != 0) {
        printf("Error: pthreads_for_A\n");
        ret = 1;
        goto out_get;
    }
    printf("A is calculated successfully\n");

    cnt_1 = 0;
    cnt_2 = 0;
    for (i = 0; i < nV; i++) {
        if (valid_vertex[i] == 1) {
            for (j = 0; j < nV; j++) {
                if (valid_vertex[j] == 1) {
                    A_valid[cnt_1 * nV_valid + cnt_2] = A[i * nV + j];
                    cnt_2++;
                }
            }
            cnt_1++;
            cnt_2 = 0;
        }
    }
    printf("A_valid is calculated successfully\n");

    C = malloc(sizeof(long double) * nV);
    if (!C) {
        printf("Error: Can't get C\n");
        ret = 1;
        goto out_get;
    }

    C_valid = malloc(sizeof(long double) * nV_valid);
    if (!C_valid) {
        printf("Error: Can't get A_valid\n");
        ret = 1;
        goto out_get;
    }

    if (pthreads_for_C(face, nV, vertex, vertexes_belong_to_faces, &H, C,
            permeability) != 0) {
        printf("Error: pthreads_for_C\n");
        ret = 1;
        goto out_get;
    }
    printf("C is calculated successfully\n");

    cnt_1 = 0;
    for (i = 0; i < nV; i++) {
        if (valid_vertex[i] == 1) {
            C_valid[cnt_1] = C[i];
            cnt_1++;
        }
    }
    printf("C_valid is calculated successfully\n");

    X = get_X(A_valid, C_valid, nV_valid);
    if (!X) {
        printf("Error: Can't get X\n");
        ret = 1;
        goto out_get;
    }
    printf("X is calculated successfully\n");

    X_out = malloc(nV_valid * sizeof(long double));
    inspection = 0;
    j = 0;
    for (i = 0; i < nV_valid; i++) {
        inspection = inspection + gsl_vector_get(X, i);
        X_out[i] = (long double) gsl_vector_get(X, i);
    }
    printf("inspection = %LF\n", inspection);

    fprintf(file, "nV\n");
    fprintf(file, "%u\n", nV);
    fprintf(file, "nV_valid\n");
    fprintf(file, "%u\n", nV_valid);
    fprintf(file, "valid_vertex\n");
    for (i = 0; i < nV; i++) {
        fprintf(file, "%u\n", valid_vertex[i]);
    }

    fprintf(file, "vertex\n");
    for (i = 0; i < nV; i++) {
        if (valid_vertex[i] == 1) {
            temp = read_coordvertex(vertex, i);
            fprintf(file, "%LF\t%LF\t%LF\n", temp.x, temp.y, temp.z);
        }
    }

    fprintf(file, "X\n");
    for (i = 0; i < nV_valid; i++) {
        fprintf(file, "%LF\n", X_out[i]);
    }

    fflush(file);

    sprintf(str, "%s/mbem_field -m%u -t%u -p%u -x%LF -y%LF -z%LF -a%LF -b%LF -c%LF "
            "-l%LF -w%LF -N%u -n%u -A%s -g%LF -r%u -u%u %s", path, flag_ambient_field,
            format_txt, format_pos, H.x, H.y, H.z, position_plane.x, position_plane.y,
            position_plane.z, size_length_plane, size_width_plane, nPoint_l_plane,
            nPoint_w_plane, Axis_plane, position_line_width_plane, nPoint_graph,
            graph_result_type, fname);
    printf("%s\n", str);

    do {
        ret = system(str);
        ret = WEXITSTATUS(ret);
        printf("mbem_potential: ret = %d\n", ret);
        if (WIFSIGNALED(ret) && (WTERMSIG(ret) == SIGINT || WTERMSIG(ret) == SIGQUIT)) {
            break;
        }
    } while (0);

out_get:
    free(vertex);
    free(tetra);
    free(tetra_material);
    free(face);
    free(face_material);
    free(vertexes_belong_to_faces);
    gsl_vector_free(X);

    if (ret != 0) {
        unlink(fname);
    } else {
        if (mesh_file) {
            fclose(mesh_file);
        }
        if (file) {
            fclose(file);
        }
    }

    return ret;
}
