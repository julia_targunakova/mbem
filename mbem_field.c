/*
 * Copyright (C) (2018) Julia Targunakova <julia.targunakova@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <assert.h>
#include <errno.h>
#include <float.h>
#include <getopt.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct vertex_simple {
    long double x;
    long double y;
    long double z;
};

struct vector_simple {
    long double x;
    long double y;
    long double z;
};

struct vertex_simple *read_vertex(FILE *file, unsigned int *nV_out)
{
    unsigned int *valid = NULL;
    char buf[256];
    struct vertex_simple *vertex = NULL;
    unsigned int i, j;
    unsigned int nV, nV_valid;

    assert(file);
    assert(nV_out);

    if (!fgets(buf, 256, file)) {
        goto bad_file;
    }

    if (!fgets(buf, 256, file)) {
        goto bad_file;
    }

    if (sscanf(buf, "%u", &nV) != 1) {
        goto bad_file;
    }
	valid = malloc(nV * sizeof(unsigned int));
    if (!valid) {
        printf("Error: failed to allocate memory for 'valid'\n");
        return NULL;
    }

    if (!fgets(buf, 256, file)) {
        goto out_valid;
    }

    if (!fgets(buf, 256, file)) {
        goto out_valid;
    }

    if (sscanf(buf, "%u", &nV_valid) != 1) {
        goto out_valid;
    }

    if (!fgets(buf, 256, file)) {
        goto out_valid;
    }

    for (i = 0; i < nV; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_valid;
        }
        if (sscanf(buf, "%u", &valid[i]) != 1) {
            goto out_valid;
        }
    }

    vertex = malloc(nV_valid * sizeof(struct vertex_simple));
    if (!vertex) {
        printf("Error: %s(): failed to allocate memory for vertexes\n", __func__);
        free(valid);
        return NULL;
    }

    j = 0;
    if (!fgets(buf, 256, file)) {
        goto out_vertex;
    }

    for (i = 0; i < nV; i++) {
        if (valid[i] == 1) {
            if (!fgets(buf, 256, file)) {
                goto out_vertex;
            }
            if (sscanf(buf, "%LF %LF %LF", &vertex[j].x, &vertex[j].y, &vertex[j].z) != 3) {
                goto out_vertex;
            }
            j++;
        }
    }

    *nV_out = nV_valid;
    free(valid);
    return vertex;

out_vertex:
    free(vertex);

out_valid:
    free(valid);

bad_file:
    printf("Error: %s(): bad input file format\n", __func__);
    return NULL;
}

long double *read_X(FILE *file, unsigned int nV)
{
    char buf[256];
    long double *x;
    unsigned int i, j;
    unsigned int divider_order;
    unsigned int divider_order_max = 0;
    long double tmp, divider, min_val, max_val, align;

    assert(file);
    assert(nV);

    x = malloc(nV * sizeof(long double));
    if (!x) {
        printf("Error: %s(): failed to allocate memory for x\n", __func__);
        return NULL;
    }

    if (!fgets(buf, 256, file)) {
        goto out_malloc;
    }

    for (i = 0; i < nV; i++) {
        if (!fgets(buf, 256, file)) {
            goto out_malloc;
        }
        if (sscanf(buf, "%Lf", &x[i]) != 1) {
            printf("sscanf() error\n");
            goto out_malloc;
        }
    }

    for (i = 0; i < nV; i++) {
        if (fabsl(x[i]) >= 10.0) {
            // 17 is empirically calculated maximum order of X
            divider_order = 17;
            divider = powl(10, divider_order);
            for (j = 0; j < 17; j++) {
                if (fabsl(x[i]) < divider) {
                    divider_order = 17 - j;
                    divider = powl(10, divider_order);
                } else {
                    break;
                }
            }
            if (divider_order_max < divider_order) {
                divider_order_max = divider_order;
            }
        }
    }
    printf("divider_order_max = %u\n", divider_order_max);

    if (divider_order_max > 1) {
        divider = powl(10, divider_order_max + 1);
        printf("X is large. Therefore X divide into %LF/n", divider);
        for (i = 0; i < nV; i++) {
            tmp = x[i];
            x[i] = tmp / divider;
        }
    }

    tmp = x[0];
    for (i = 1; i < nV; i++) {
        if (tmp > x[i]) {
            tmp = x[i];
        }
    }
    min_val = tmp;
    printf ("min value = %LF\n", tmp);

    tmp = x[0];
    for (i = 1; i < nV; i++) {
        if (tmp < x[i]) {
            tmp = x[i];
        }
    }
    max_val = tmp;
    printf ("max value = %LF\n", tmp);

    if (min_val < 0) {
        tmp = fabsl(min_val) + max_val;
    } else {
        tmp = min_val + max_val;
    }
    align = tmp / 2;
    tmp = max_val - align;
    for (i = 0; i < nV; i++) {
        align = x[i];
        x[i] = align - tmp;
    }

    return x;
out_malloc:
    free(x);
    printf("Error: %s(): bad input file format\n", __func__);
    return NULL;
}

struct vector_simple calc_coordvector(struct vertex_simple *v1, struct vertex_simple *v2)
{
    struct vector_simple coordvector;
    coordvector.x = v2->x - v1->x;
    coordvector.y = v2->y - v1->y;
    coordvector.z = v2->z - v1->z;
    return coordvector;
}

long double calc_vectorlength(struct vector_simple *v1)
{
    long double vectorlength;
    vectorlength = sqrtl(powl(v1->x, 2) + powl(v1->y, 2) + powl(v1->z, 2));
    return vectorlength;
}

long double calc_potential_point(long double *X, struct vertex_simple *vertex,
        struct vertex_simple *w, unsigned int nV)
{
    long double potential = 0;
    unsigned int i;
    struct vector_simple dist;
	long double l_dist;

    for (i = 0; i < nV; i++) {
        dist = calc_coordvector(&vertex[i], w);
        l_dist = calc_vectorlength(&dist);
        if (abs(l_dist) >= 1) {
            potential = potential + X[i] / l_dist;
        }
    }
    potential = potential / (M_PI * 4.0);
    return potential;
}

struct vector_simple calc_intensity_point(long double *X,
        struct vertex_simple *vertex, struct vertex_simple *w, unsigned int nV)
{
    struct vector_simple intenst, dist;
    long double distance_cubed;
    unsigned int i;
    intenst.x = 0;
    intenst.y = 0;
    intenst.z = 0;
    for (i = 0; i < nV; i++) {
        dist = calc_coordvector(&vertex[i], w);
        distance_cubed = powl((powl(dist.x, 2.0) + powl(dist.y, 2.0) + powl(dist.z, 2.0)),
                    3.0 / 2.0);
        if (fabsl(distance_cubed) >= 0.01) {
            intenst.x = intenst.x + (X[i] * dist.x / distance_cubed);
            intenst.y = intenst.y + (X[i] * dist.y / distance_cubed);
            intenst.z = intenst.z + (X[i] * dist.z / distance_cubed);
        }
    }
    intenst.x = intenst.x / ((-1.0) * M_PI * 4.0);
    intenst.y = intenst.y / ((-1.0) * M_PI * 4.0);
    intenst.z = intenst.z / ((-1.0) * M_PI * 4.0);
    return intenst;
}

struct vector_simple calc_induction_point(long double *X,
        struct vertex_simple *vertex, struct vertex_simple *w, unsigned int nV)
{
    struct vector_simple induction;
    induction = calc_intensity_point(X, vertex, w, nV);
    induction.x *= 1.2567;
    induction.y *= 1.2567;
    induction.z *= 1.2567;
    return induction;
}

int calc_magn_field(int format_pos, int format_txt, struct vertex_simple *w,
        struct vertex_simple *vertex, long double *X, long double size_l,
        long double size_w, int nPoint_l, int nPoint_w, int nPoint_graph,
        unsigned int nV, struct vector_simple *ambient_field,
        long double position_line_width, int graph_result_type, char *Axis)
{
    unsigned int i, j;
    long double dl, dw, t_w = 0, t_l = 0;
    char str[400];
    long double u = 0, module_intensity = 0, module_induction = 0;
    struct vector_simple intensity[nPoint_l][nPoint_w];
    struct vector_simple value_field;
    FILE *file_field_overlays_x_y_pos = NULL;
    char *fname_field_overlays_x_y_pos = "intensity_field_overlays_x_y.pos";
    FILE *file_field_overlays_z_y_pos = NULL;
    char *fname_field_overlays_z_y_pos = "intensity_field_overlays_z_y.pos";
    FILE *file_field_overlays_z_x_pos = NULL;
    char *fname_field_overlays_z_x_pos = "intensity_field_overlays_z_x.pos";

    if (Axis[0] == 'z' || Axis[0] == 'Z') {
        file_field_overlays_x_y_pos = fopen(fname_field_overlays_x_y_pos, "w");
        if (file_field_overlays_x_y_pos == NULL) {
            printf("Error opening file intensity_field_overlays_x_y.pos");
            return -1;
        }
    } else if (Axis[0] == 'y' || Axis[0] == 'Y') {
        file_field_overlays_z_x_pos = fopen(fname_field_overlays_z_x_pos, "w");
        if (file_field_overlays_z_x_pos == NULL) {
            printf("Error opening file intensity_field_overlays_z_x.pos");
            return -1;
        }
    } else if (Axis[0] == 'x' || Axis[0] == 'X') {
        file_field_overlays_z_y_pos = fopen(fname_field_overlays_z_y_pos, "w");
        if (file_field_overlays_z_y_pos == NULL) {
            printf("Error opening file intensity_field_overlays_z_y.pos");
            return -1;
        }
    } else {
        printf("Error: Wrong Axis - %s\n", Axis);
        return -1;
    }
    FILE *file_graph_intensity_pos;
    char *fname_graph_intensity_pos = "intensity_line_overlays_out.pos";
    file_graph_intensity_pos = fopen(fname_graph_intensity_pos, "w");
    if (file_graph_intensity_pos == NULL) {
        printf("Error opening file intensity_line_overlays_out.pos");
        return -1;
    }
    FILE *file_graph_potential_pos;
    char *fname_graph_potential_pos = "potential_line_overlays_out.pos";
    file_graph_potential_pos = fopen(fname_graph_potential_pos, "w");
    if (file_graph_potential_pos == NULL) {
        printf("Error opening file potential_line_overlays_out.pos");
        return -1;
    }
    FILE *file_graph_induction_pos;
    char *fname_graph_induction_pos = "induction_line_overlays_out.pos";
    file_graph_induction_pos = fopen(fname_graph_induction_pos, "w");
    if (file_graph_induction_pos == NULL) {
        printf("Error opening file induction_line_overlays_out.pos");
        return -1;
    }
    FILE *file_graph_induction_x_pos;
    char *fname_graph_induction_x_pos = "induction_graph_x.pos";
    file_graph_induction_x_pos = fopen(fname_graph_induction_x_pos, "w");
    if (file_graph_induction_x_pos == NULL) {
        printf("Error opening file induction_graph_x.pos");
        return -1;
    }
    FILE *file_graph_induction_y_pos;
    char *fname_graph_induction_y_pos = "induction_graph_y.pos";
    file_graph_induction_y_pos = fopen(fname_graph_induction_y_pos, "w");
    if (file_graph_induction_y_pos == NULL) {
        printf("Error opening file induction_graph_y.pos");
        return -1;
    }
    FILE *file_graph_induction_z_pos;
    char *fname_graph_induction_z_pos = "induction_graph_z.pos";
    file_graph_induction_z_pos = fopen(fname_graph_induction_z_pos, "w");
    if (file_graph_induction_z_pos == NULL) {
        printf("Error opening file induction_graph_z.pos");
        return -1;
    }
    FILE *file_graph_intensity_txt;
    char *fname_graph_intensity_txt = "intensity_line_overlays_out.txt";
    file_graph_intensity_txt = fopen(fname_graph_intensity_txt, "w");
    if (file_graph_intensity_txt == NULL) {
        printf("Error opening file intensity_line_overlays_out.txt");
        return -1;
    }
    FILE *file_graph_potential_txt;
    char *fname_graph_potential_txt = "potential_line_overlays_out.txt";
    file_graph_potential_txt = fopen(fname_graph_potential_txt, "w");
    if (file_graph_potential_txt == NULL) {
        printf("Error opening file potential_line_overlays_out.txt");
        return -1;
    }
    FILE *file_graph_induction_txt;
    char *fname_graph_induction_txt = "induction_line_overlays_out.txt";
    file_graph_induction_txt = fopen(fname_graph_induction_txt, "w");
    if (file_graph_induction_txt == NULL) {
        printf("Error opening file induction_line_overlays_out.txt");
        return -1;
    }
    FILE *file_print_X;
    char *fname_print_X = "print_X.pos";
    file_print_X = fopen(fname_print_X, "w");
    if (file_print_X == NULL) {
        printf("Error opening file print_X.pos");
        return -1;
    }

    if (format_pos == 1) {
        if (graph_result_type == 1) {
            sprintf(str, "View \"Um\" {");
            fprintf(file_graph_potential_pos, "%s\n", str);
            printf("Choose potential_line_overlays_out.pos\n");
        } else if (graph_result_type == 0) {
            sprintf(str, "View \"Hm[A/m]\" {");
            fprintf(file_graph_intensity_pos, "%s\n", str);
            printf("Choose intensity_line_overlays_out.pos\n");
        } else if (graph_result_type == 2) {
            sprintf(str, "View \"Bm[uT]\" {");
            fprintf(file_graph_induction_pos, "%s\n", str);
            printf("Choose induction_line_overlays_out.pos\n");
            sprintf(str, "View \"Bm_x[uT]\" {");
            fprintf(file_graph_induction_x_pos, "%s\n", str);
            sprintf(str, "View \"Bm_y[uT]\" {");
            fprintf(file_graph_induction_y_pos, "%s\n", str);
            sprintf(str, "View \"Bm_z[uT]\" {");
            fprintf(file_graph_induction_z_pos, "%s\n", str);
        }
        if (Axis[0] == 'z' || Axis[0] == 'Z') {
            sprintf(str, "View \"Hm_Vector_Axis_Z\" {");
            fprintf(file_field_overlays_x_y_pos, "%s\n", str);
            printf("The field is displayed perpendicular to the axis Z\n");
        } else if (Axis[0] == 'y' || Axis[0] == 'Y') {
            sprintf(str, "View \"Hm_Vector_Axis_Y\" {");
            fprintf(file_field_overlays_z_x_pos, "%s\n", str);
            printf("The field is displayed perpendicular to the axis Y\n");
        } else if (Axis[0] == 'x' || Axis[0] == 'X') {
            sprintf(str, "View \"Hm_Vector_Axis_X\" {");
            fprintf(file_field_overlays_z_y_pos, "%s\n", str);
            printf("The field is displayed perpendicular to the axis X\n");
        } else {
            printf("Error: Wrong Axis[0] value\n");
            return -1;
        }
    }
    if (format_txt == 1) {
        if (graph_result_type == 1) {
            fprintf(file_graph_potential_txt, "potential\n");
        } else if (graph_result_type == 0) {
            fprintf(file_graph_intensity_txt, "modul_intensity\n");
        } else if (graph_result_type == 2) {
            fprintf(file_graph_induction_txt, "modul_induction\n");
        } else {
            printf("Error: Wrong graph_result_type value - %d\n", graph_result_type);
            return -1;
        }
    }

    if (Axis[0] == 'z' || Axis[0] == 'Z') {
        t_l = w->x;
        t_w = w->y;
    } else if (Axis[0] == 'y' || Axis[0] == 'Y') {
        t_l = w->x;
        t_w = w->z;
    } else if (Axis[0] == 'x' || Axis[0] == 'X') {
        t_l = w->y;
        t_w = w->z;
    }
    dl = size_l / (nPoint_l - 1);
    dw = size_w / (nPoint_w - 1);
    printf("t_l = %LF\t, t_w = %LF\t, dl =  %LF\t, dw =  %LF\n", t_l, t_w, dl, dw);
    for (i = 0; i < (nPoint_l); i++) {
        for (j = 0; j < (nPoint_w); j++) {
            intensity[i][j] = calc_intensity_point(X, vertex, w, nV);
            if (format_pos == 1) {
                sprintf(str, "VP (%LF, %LF, %LF) {%LF,%LF,%LF};\n", w->x, w->y,
                        w->z, intensity[i][j].x + ambient_field->x, intensity[i][j].y
                        + ambient_field->y, intensity[i][j].z + ambient_field->z);
                if (Axis[0] == 'z' || Axis[0] == 'Z') {
                    fprintf(file_field_overlays_x_y_pos, "%s", str);
                } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
                    fprintf(file_field_overlays_z_x_pos, "%s", str);
                } else if (Axis[0] == 'X' || Axis[0] == 'x') {
                    fprintf(file_field_overlays_z_y_pos, "%s", str);
                }
            }
            if (Axis[0] == 'z' || Axis[0] == 'Z') {
                w->y = w->y + dw;
            } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
                w->z = w->z + dw;
            } else if (Axis[0] == 'X' || Axis[0] == 'x') {
                w->z = w->z + dw;
            }
        }
        if (Axis[0] == 'z' || Axis[0] == 'Z') {
            w->y = t_w;
            w->x = w->x + dl;
        } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
            w->z = t_w;
            w->x = w->x + dl;
        } else if (Axis[0] == 'X' || Axis[0] == 'x') {
            w->z = t_w;
            w->y = w->y + dl;
        }
    }
    if (format_pos == 1) {
        sprintf(str, "};");
        if (Axis[0] == 'z' || Axis[0] == 'Z') {
            fprintf(file_field_overlays_x_y_pos, "%s\n", str);
        } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
            fprintf(file_field_overlays_z_x_pos, "%s", str);
        } else if (Axis[0] == 'X' || Axis[0] == 'x') {
            fprintf(file_field_overlays_z_y_pos, "%s", str);
        }
        printf("%s: Result is written to the"
                " intensity_field_overlays.pos successfully\n", __func__);
    }
    dl = size_l / (nPoint_graph - 1);
    printf("size_l  = %LF\t, nPoint_graph  = %d\t, dl =  %LF\n", size_l,
        nPoint_graph, dl);
    if (Axis[0] == 'z' || Axis[0] == 'Z') {
        w->x = t_l;
        w->y = position_line_width;
    } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
        w->x = t_l;
        w->z = position_line_width;
    } else if (Axis[0] == 'X' || Axis[0] == 'x') {
        w->y = t_l;
        w->z = position_line_width;
    }
    for (i = 0; i < nPoint_graph; i++) {
        if (graph_result_type == 1) {
            u = calc_potential_point(X, vertex, w, nV);
            if (format_pos == 1) {
                if (Axis[0] == 'z' || Axis[0] == 'Z') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x, u);
                } else if (Axis[0] == 'X' || Axis[0] == 'x') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y, u);
                } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x, u);
                }
                printf("w.x = %LF\tw.y = %LF\tw.z = %LF\t u = %LF\n", w->x,
                        w->y, w->z, u);
                printf("%s\n", str);
                fprintf(file_graph_potential_pos, "%s", str);
            } else if (format_txt == 1) {
                fprintf(file_graph_potential_txt, "%LF\n", u);
            }
        } else if (graph_result_type == 0) {
            value_field = calc_intensity_point(X, vertex, w, nV);
            module_intensity = sqrtl(powl((value_field.x + ambient_field->x), 2)
                    + powl((value_field.y + ambient_field->y), 2) +
                    powl((value_field.z + ambient_field->z), 2));
            if (format_pos == 1) {
                if (Axis[0] == 'z' || Axis[0] == 'Z') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            module_intensity);
                } else if (Axis[0] == 'X' || Axis[0] == 'x') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y,
                            module_intensity);
                } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            module_intensity);
                }
                fprintf(file_graph_intensity_pos, "%s", str);
            } else if (format_txt == 1) {
                fprintf(file_graph_intensity_txt, "%LF\n", module_intensity);
            }
        } else if (graph_result_type == 2) {
            value_field = calc_induction_point(X, vertex, w, nV);
            module_induction = sqrtl(powl((value_field.x + ambient_field->x * 1.2567), 2)
                    + powl((value_field.y + ambient_field->y * 1.2567), 2) +
                    powl((value_field.z + ambient_field->z * 1.2567), 2));
            if (format_pos == 1) {
                if (Axis[0] == 'z' || Axis[0] == 'Z') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            module_induction);
                    fprintf(file_graph_induction_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.x);
                    fprintf(file_graph_induction_x_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.y);
                    fprintf(file_graph_induction_y_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.z);
                    fprintf(file_graph_induction_z_pos, "%s", str);
                } else if (Axis[0] == 'X' || Axis[0] == 'x') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y,
                            module_induction);
                    fprintf(file_graph_induction_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y,
                            value_field.x);
                    fprintf(file_graph_induction_x_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y,
                            value_field.y);
                    fprintf(file_graph_induction_y_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->y,
                            value_field.z);
                    fprintf(file_graph_induction_z_pos, "%s", str);
                } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            module_induction);
                    fprintf(file_graph_induction_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.x);
                    fprintf(file_graph_induction_x_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.y);
                    fprintf(file_graph_induction_y_pos, "%s", str);
                    sprintf(str, "SP (0, %LF, 0) {%LF};\n", w->x,
                            value_field.z);
                    fprintf(file_graph_induction_z_pos, "%s", str);
                }
            }
        } else if (format_txt == 1) {
            fprintf(file_graph_induction_txt, "%LF\n", module_induction);
        }
        if (Axis[0] == 'z' || Axis[0] == 'Z') {
            w->x = w->x + dl;
        } else if (Axis[0] == 'Y' || Axis[0] == 'y') {
            w->x = w->x + dl;
        } else if (Axis[0] == 'X' || Axis[0] == 'x') {
            w->y = w->y + dl;
        }
    }
    if (format_pos == 1) {
        sprintf(str, "};");
        if (graph_result_type == 0) {
            fprintf(file_graph_intensity_pos, "%s\n", str);
        } else if (graph_result_type == 1) {
            fprintf(file_graph_potential_pos, "%s\n", str);
        } else if (graph_result_type == 2) {
            fprintf(file_graph_induction_pos, "%s\n", str);
            fprintf(file_graph_induction_x_pos, "%s\n", str);
            fprintf(file_graph_induction_y_pos, "%s\n", str);
            fprintf(file_graph_induction_z_pos, "%s\n", str);
        }
    }

    if (format_pos == 1) {
        fprintf(file_print_X, "View \"X\" {\n");
        for (i = 0; i < nV; i++) {
            fprintf(file_print_X, "SP (%LF, %LF, %LF) {%LF};\n", vertex[i].x,
        vertex[i].y, vertex[i].z, X[i]);
        }
        fprintf(file_print_X, "}; \n");
        printf("cal_field_universal: Result is written to the print_X.pos\n");
    }
    if (file_field_overlays_x_y_pos) {
        fclose(file_field_overlays_x_y_pos);
    }
    if (file_field_overlays_z_y_pos) {
        fclose(file_field_overlays_z_y_pos);
    }
    if (file_field_overlays_z_x_pos) {
        fclose(file_field_overlays_z_x_pos);
    }
    if (file_graph_intensity_pos) {
        fclose(file_graph_intensity_pos);
    }
    if (file_graph_potential_pos) {
        fclose(file_graph_potential_pos);
    }
    if (file_graph_intensity_txt) {
        fclose(file_graph_intensity_txt);
    }
    if (file_graph_potential_txt) {
        fclose(file_graph_potential_txt);
    }
    if (file_print_X) {
        fclose(file_print_X);
    }
    if (file_graph_induction_x_pos) {
        fclose(file_graph_induction_x_pos);
    }
    if (file_graph_induction_y_pos) {
        fclose(file_graph_induction_y_pos);
    }
    if (file_graph_induction_z_pos) {
        fclose(file_graph_induction_z_pos);
    }
    return 0;
}

int main(int argc, char **argv)
{
    const char *short_options = "m:t:p:x:y:z:a:b:c:l:w:N:n:A:g:r:u:";
    int ret = 0;
    int option_index = -1;
    int flag_output_format_txt = 0;
    int flag_output_format_pos = 0;

    struct vertex_simple w;

    long double *X = NULL;
    struct vertex_simple *vertex = NULL;
    unsigned int nV = 0;

    int flag_ambient_field = 0;
    unsigned int graph_result_type = 0;

    long double Hx = 0;
    long double Hy = 0;
    long double Hz = 0;
    struct vector_simple ambient_field;
    ambient_field.x = 0;
    ambient_field.y = 0;
    ambient_field.z = 0;

    long double size_length = 0;
    long double size_width = 0;
    int nPoint_l = 10;
    int nPoint_w = 10;
    int nPoint_graph = 161;
    long double position_line_width = 0;
    char *Axis = "Z";
    char *fname_in = 0;
    printf("mbem_field working:\n");

    const struct option long_options[] = {
            { "output_format_txt", optional_argument, NULL, 't' },
            { "output_format_pos", optional_argument, NULL, 'p' },
            { "Hx", optional_argument, NULL, 'x' },
            { "Hy", optional_argument, NULL, 'y' },
            { "Hz", optional_argument, NULL, 'z' },
            { "position_x", optional_argument, NULL, 'a' },
            { "position_y", optional_argument, NULL, 'b' },
            { "position_z", optional_argument, NULL, 'c' },
            { "size_length", optional_argument, NULL, 'l' },
            { "size_width", optional_argument, NULL, 'w' },
            { "nPoint_l", optional_argument, NULL, 'N' },
            { "nPoint_w", optional_argument, NULL, 'n' },
            { "Axis", optional_argument, NULL, 'A' },
            { "position_line_width", optional_argument, NULL, 'g' },
            { "nPoint_graph", optional_argument, NULL, 'r' },
            { "graph_result_type", optional_argument, NULL, 'u' },
            { "flag_ambient_field", optional_argument, NULL, 'm' },
            { NULL, 0, NULL, 0 } };

    while ((ret = getopt_long(argc, argv, short_options, long_options,
        &option_index)) != -1) {
        switch (ret) {
        case 't':
            if (optarg != NULL) {
                flag_output_format_txt = atoi(optarg);
                printf("flag_output_format_txt = %d\n", flag_output_format_txt);
            } else {
                printf("error!\n");
            }
            break;
        case 'p':
            if (optarg != NULL) {
                flag_output_format_pos = atoi(optarg);
                printf("flag_output_format_pos = %d\n", flag_output_format_pos);
            } else {
                printf("error!\n");
            }
            break;
        case 'x':
            if (optarg != NULL) {
                Hx = (long double) atof(optarg);
                printf("Hx = %LF\n", Hx);
            } else {
                printf("error!\n");
            }
            break;
        case 'y':
            if (optarg != NULL) {
                Hy = (long double) atof(optarg);
                printf("Hy = %LF\n", Hy);
            } else {
                printf("error!\n");
            }
            break;
        case 'z':
            if (optarg != NULL) {
                Hz = (long double) atof(optarg);
                printf("Hz = %LF\n", Hz);
            } else {
                printf("error!\n");
            }
            break;
        case 'a':
            if (optarg != NULL) {
                w.x = (long double) atof(optarg);
                printf("starting position x (plane) = %LF\n", w.x);
            } else {
                printf("error!\n");
            }
            break;
        case 'b':
            if (optarg != NULL) {
                w.y = (long double) atof(optarg);
                printf("starting position y (plane) = %LF\n", w.y);
            } else {
                printf("error!\n");
            }
            break;
        case 'c':
            if (optarg != NULL) {
                w.z = (long double) atof(optarg);
                printf("starting position z (plane) = %LF\n", w.z);
            } else {
                printf("error!\n");
            }
            break;
        case 'l':
            if (optarg != NULL) {
                size_length = (long double) atof(optarg);
                printf("size length of plane = %LF\n", size_length);
            } else {
                printf("error!\n");
            }
            break;
        case 'w':
            if (optarg != NULL) {
                size_width = (long double) atof(optarg);
                printf("size width of plane  = %LF\n", size_width);
            } else {
                printf("error!\n");
            }
            break;
        case 'N':
            if (optarg != NULL) {
                nPoint_l = (long double) atof(optarg);
                printf("nPoint_l = %d\n", nPoint_l);
            } else {
                printf("error!\n");
            }
            break;
        case 'n':
            if (optarg != NULL) {
                nPoint_w = (long double) atof(optarg);
                printf("nPoint_w = %d\n", nPoint_w);
            } else {
                printf("error!\n");
            }
            break;
        case 'g':
            if (optarg != NULL) {
                position_line_width = (long double) atof(optarg);
                printf("position_line_width = %LF\n", position_line_width);
            } else {
                printf("error!\n");
            }
            break;
        case 'A':
            if (optarg != NULL) {
                Axis = optarg;
                printf("Axis = %s\n", Axis);
            } else {
                printf("error!\n");
            }
            break;
        case 'r':
            if (optarg != NULL) {
                nPoint_graph = (long double) atof(optarg);
                printf("nPoint_graph = %d\n", nPoint_graph);
            } else {
                printf("error!\n");
            }
            break;
        case 'u':
            if (optarg != NULL) {
                graph_result_type = atoi(optarg);
                if (graph_result_type == 0)
                    printf("Output result intensity\n");
                else if (graph_result_type == 1)
                    printf("Output result potential\n");
                else if (graph_result_type == 2)
                    printf("Output result induction\n");
                else {
                    printf("Error: graph_result_type - %d\n",
                            graph_result_type);
                    return -1;
                }
            } else {
                printf("error!\n");
            }
            break;
        case 'm':
            if (optarg != NULL) {
                flag_ambient_field = atoi(optarg);
                if (flag_ambient_field != 0) {
                    printf("Output result result field\n");
                } else {
                    printf("Output result induced field\n");
                }
            } else {
                printf("error!\n");
            }
            break;
        case '?':
        default:
            printf("found unknown option\n");
            break;
        }
        option_index = -1; // init with error value
    }

    if (argc != 19) {
        printf("Error: wrong number of arguments\n");
        printf("-m or --flag_ambient_field\n");
        printf("-t or --output_format_txt if 1 output the result in a format txt\n");
        printf("-p or --output_format_pos if 1 output the result in a format msh.pos\n");
        printf("-x or --Hx value ambient_field_x\n");
        printf("-y or --Hy value ambient_field_y\n");
        printf("-z or --Hz value ambient_field_z\n");
        printf("-a or --w_x starting position x (plane)\n");
        printf("-b or --w_y starting position y (plane)\n");
        printf("-c or --w_z starting position z (plane)\n");
        printf("-l or --size length\n");
        printf("-w or --size width\n");
        printf("-N or --number of points along the length\n");
        printf("-n or --number of points along the width\n");
        printf("-A or --plane parallel to axis\n");
        printf("-g or --position_line_width\n");
        printf("-r or --nPoint_graph\n");
        printf("-u or --graph_result_type\n");
        return 1;
    } else {
        fname_in = argv[18];
        printf("fname_in = %s\n", fname_in);
    }

    FILE *file_in;
    file_in = fopen(fname_in, "r");
    if (file_in == NULL) {
        printf("Can't open file '%s'\n", fname_in);
        return 1;
    }

    vertex = read_vertex(file_in, &nV);
    if (!vertex) {
        printf("Error: Can't get vertexes\n");
        ret = 1;
        goto out_get;
    }

    X = read_X(file_in, nV);
    if (!X) {
        printf("Error: Can't get X\n");
        ret = 1;
        goto out_get;
    }

    if (flag_ambient_field != 0) {
        ambient_field.x = Hx;
        ambient_field.y = Hy;
        ambient_field.z = Hz;
    }

    ret = calc_magn_field(flag_output_format_pos, flag_output_format_txt, &w,
            vertex, X, size_length, size_width, nPoint_l, nPoint_w,nPoint_graph,
            nV, &ambient_field, position_line_width, graph_result_type, Axis);
    if (ret) {
        printf("Error: mbem_field failed - %d\n", ret);
        goto out_get;
    } else {
        printf("mbem_field succeeded\n");
    }

    ret = 0;
out_get:
    if (vertex) {
        free(vertex);
    }
    if (X) {
        free(X);
    }
    if (file_in) {
        fclose(file_in);
    }
    return ret;
}
